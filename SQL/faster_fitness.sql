-- phpMyAdmin SQL Dump
-- version 4.4.7
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Sep 14, 2015 at 05:22 AM
-- Server version: 5.6.25
-- PHP Version: 5.5.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `faster_fitness`
--

-- --------------------------------------------------------

--
-- Table structure for table `ff_answer`
--

CREATE TABLE IF NOT EXISTS `ff_answer` (
  `AnswerID` int(11) NOT NULL,
  `AnswerMID` int(11) NOT NULL,
  `AnswerGID` int(11) NOT NULL,
  `AnswerEID` int(11) NOT NULL,
  `AnswerText` varchar(250) NOT NULL,
  `AnswerStatus` enum('Pending','Approve','Reject') NOT NULL DEFAULT 'Pending',
  `ScheduleDate` date NOT NULL,
  `AnswerCreatedOn` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ff_exercises`
--

CREATE TABLE IF NOT EXISTS `ff_exercises` (
  `EID` int(11) NOT NULL,
  `EName` varchar(255) NOT NULL,
  `EUnitType` varchar(255) NOT NULL,
  `EUnit` varchar(255) NOT NULL,
  `ENotes` varchar(255) NOT NULL,
  `EQuestion` varchar(250) NOT NULL,
  `ECreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ff_gifts`
--

CREATE TABLE IF NOT EXISTS `ff_gifts` (
  `GftID` int(11) NOT NULL,
  `GftName` varchar(255) NOT NULL,
  `GftNotes` varchar(255) NOT NULL,
  `GftPoints` int(11) NOT NULL,
  `GftDate` date NOT NULL,
  `GftCreated` datetime NOT NULL,
  `GftPicName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ff_groups`
--

CREATE TABLE IF NOT EXISTS `ff_groups` (
  `GID` int(11) NOT NULL,
  `GName` varchar(255) NOT NULL,
  `GDescription` longtext NOT NULL,
  `GPicName` varchar(255) NOT NULL,
  `GCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ff_leaderboard`
--

CREATE TABLE IF NOT EXISTS `ff_leaderboard` (
  `LeadID` int(11) NOT NULL,
  `LeadName` varchar(255) NOT NULL,
  `LeadPoints` int(255) NOT NULL,
  `LeadFor` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ff_members`
--

CREATE TABLE IF NOT EXISTS `ff_members` (
  `MID` int(11) NOT NULL,
  `MName` varchar(255) NOT NULL,
  `MGender` varchar(10) NOT NULL,
  `MWeight` int(11) NOT NULL,
  `MAge` int(11) NOT NULL,
  `MType` enum('New','Existing') NOT NULL DEFAULT 'Existing',
  `MNumber` varchar(15) NOT NULL,
  `MEmail` varchar(255) NOT NULL,
  `MAPPCode` varchar(10) NOT NULL,
  `MAddress` varchar(255) NOT NULL,
  `MStatus` enum('Paid','Unpaid') NOT NULL DEFAULT 'Unpaid',
  `MEarnPoint` varchar(50) NOT NULL,
  `MGpid` int(11) NOT NULL,
  `MExpireDate` varchar(255) NOT NULL,
  `MTrialDays` int(11) NOT NULL,
  `MCreated` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ff_points`
--

CREATE TABLE IF NOT EXISTS `ff_points` (
  `EarnID` int(11) NOT NULL,
  `EarnTopic` varchar(255) NOT NULL,
  `EarnPoints` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ff_points`
--

INSERT INTO `ff_points` (`EarnID`, `EarnTopic`, `EarnPoints`) VALUES
(1, 'Existing user Registration', 10),
(2, 'New User Registration', 10),
(3, 'New User Referral via SMS', 15),
(5, 'New User Refferal via Facebook', 20),
(6, 'New User Refferal via Twitter', 25),
(7, 'Free membership to PaidMembership', 10),
(8, 'Met target daily', 10),
(9, 'Refferal to Free Membership', 10);

-- --------------------------------------------------------

--
-- Table structure for table `ff_referral`
--

CREATE TABLE IF NOT EXISTS `ff_referral` (
  `RefID` int(11) NOT NULL,
  `RefAppcode` varchar(10) NOT NULL,
  `RefUserid` int(11) NOT NULL,
  `RefStatus` enum('PENDING','COMPLETE') NOT NULL DEFAULT 'PENDING',
  `RefType` enum('SMS','Facebook','Twitter') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ff_targets`
--

CREATE TABLE IF NOT EXISTS `ff_targets` (
  `TID` int(11) NOT NULL,
  `TGID` int(11) NOT NULL,
  `TEID` int(11) NOT NULL,
  `TMonday` enum('0','1') NOT NULL DEFAULT '0',
  `TTuesday` enum('0','1') NOT NULL DEFAULT '0',
  `TWednesday` enum('0','1') NOT NULL DEFAULT '0',
  `TThursday` enum('0','1') NOT NULL DEFAULT '0',
  `TFriday` enum('0','1') NOT NULL DEFAULT '0',
  `TSaturday` enum('0','1') NOT NULL DEFAULT '0',
  `TSunday` enum('0','1') NOT NULL DEFAULT '0',
  `TEUnit` varchar(50) NOT NULL DEFAULT '0',
  `TCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ff_users`
--

CREATE TABLE IF NOT EXISTS `ff_users` (
  `UID` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `CreatedOn` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ff_users`
--

INSERT INTO `ff_users` (`UID`, `Name`, `Email`, `Password`, `CreatedOn`) VALUES
(1, 'Admin', 'admin@fasterfitness.com', '39bd593cddb6fecdcf9783709a0355c01fc05061', '2015-08-10 17:09:36');

-- --------------------------------------------------------

--
-- Table structure for table `ff_weights`
--

CREATE TABLE IF NOT EXISTS `ff_weights` (
  `WID` int(11) NOT NULL,
  `MID` int(11) NOT NULL,
  `WMonthYear` date NOT NULL,
  `Weight` double NOT NULL,
  `WCreated` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ff_answer`
--
ALTER TABLE `ff_answer`
  ADD PRIMARY KEY (`AnswerID`);

--
-- Indexes for table `ff_exercises`
--
ALTER TABLE `ff_exercises`
  ADD PRIMARY KEY (`EID`);

--
-- Indexes for table `ff_gifts`
--
ALTER TABLE `ff_gifts`
  ADD PRIMARY KEY (`GftID`);

--
-- Indexes for table `ff_groups`
--
ALTER TABLE `ff_groups`
  ADD PRIMARY KEY (`GID`);

--
-- Indexes for table `ff_members`
--
ALTER TABLE `ff_members`
  ADD PRIMARY KEY (`MID`);

--
-- Indexes for table `ff_points`
--
ALTER TABLE `ff_points`
  ADD PRIMARY KEY (`EarnID`);

--
-- Indexes for table `ff_referral`
--
ALTER TABLE `ff_referral`
  ADD PRIMARY KEY (`RefID`);

--
-- Indexes for table `ff_targets`
--
ALTER TABLE `ff_targets`
  ADD PRIMARY KEY (`TID`);

--
-- Indexes for table `ff_users`
--
ALTER TABLE `ff_users`
  ADD PRIMARY KEY (`UID`),
  ADD UNIQUE KEY `Email` (`Email`);

--
-- Indexes for table `ff_weights`
--
ALTER TABLE `ff_weights`
  ADD PRIMARY KEY (`WID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ff_answer`
--
ALTER TABLE `ff_answer`
  MODIFY `AnswerID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_exercises`
--
ALTER TABLE `ff_exercises`
  MODIFY `EID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_gifts`
--
ALTER TABLE `ff_gifts`
  MODIFY `GftID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_groups`
--
ALTER TABLE `ff_groups`
  MODIFY `GID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_members`
--
ALTER TABLE `ff_members`
  MODIFY `MID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_points`
--
ALTER TABLE `ff_points`
  MODIFY `EarnID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `ff_referral`
--
ALTER TABLE `ff_referral`
  MODIFY `RefID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_targets`
--
ALTER TABLE `ff_targets`
  MODIFY `TID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ff_users`
--
ALTER TABLE `ff_users`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `ff_weights`
--
ALTER TABLE `ff_weights`
  MODIFY `WID` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
