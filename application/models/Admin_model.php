<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
    
class Admin_model extends CI_Model {
    
	// Start of Constructor
    public function __construct() {
        parent::__construct();
    }
	// End of the function
	
	// Start of logn script
	public function do_login($Email, $Password) {
		$this->db->where("Email", $Email);
		$this->db->where("Password", $Password);
		$query = $this->db->get("users");
		if ( $query->num_rows() == 1 ) {
			foreach($query->result() as $rows) {
				// add all data to session
				$newdata = array(
					'UserID'  	=> $rows->UID,
					'UserEmail'	=> $rows->Email,
					'UserName'	=> $rows->Name,
					'Loggedin' 	=> TRUE,
				);
			}
			$this->session->set_userdata($newdata);
			return 1;
		} else {
			return 0;
		}
    }
	// End of the function
	
	// Start of insert member script
	public function insert_member($MemberData) {
		if ( !empty($MemberData) ) {
			$query = $this->db->set('MCreated', 'NOW()', FALSE)->insert('members', $MemberData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of insert member script
	public function insert_member_weight($MID, $MWeight) {
		$MemberData = array(
			'MID' => $MID,
			'WMonthYear' => date('Y-m-d'),
			'Weight' => $MWeight,
		);
		$query = $this->db->set('WCreated', 'NOW()', FALSE)->insert('weights', $MemberData);
		if ( $query ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
	
	// Start of insert new member script
	public function insert_new_member($MemberData) {
		if ( !empty($MemberData) ) {
			$query = $this->db->set('MCreated', 'NOW()', FALSE)->insert('members', $MemberData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	public function get_member_weights($MID) {
		$this->db->where("MID", $MID);
		$query = $this->db->get("weights");
		return $query->result();
	}
	
	
	// Start of insert exercise script
	public function insert_exercise($ExerciseData) {
		if ( !empty($ExerciseData) ) {
			$query = $this->db->set('ECreated', 'NOW()', FALSE)->insert('exercises', $ExerciseData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of insert exercise script
	public function insert_nexercise($HomeExerciseData) {
		if ( !empty($HomeExerciseData) ) {
			$query = $this->db->set('NECreated', 'NOW()', FALSE)->insert('exercise', $HomeExerciseData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of get all exercises script
	public function get_exercises() {
		$this->db->order_by("EID", "DESC");
		$query = $this->db->get("exercises");
		return $query->result();
	}
	// End of the function
	
	
	// Start of get all exercises script
	public function get_targets_by_day($EID, $GID, $day) {
		$this->db->where('TEID', $EID);
		$this->db->where('TGID', $GID);
		$query = $this->db->get("targets");
		if ( $query->num_rows() != 0 ) {
			$this->db->where('TEID', $EID);
			$this->db->where('TGID', $GID);
			$query = $this->db->get('targets');
			return $query->result();
        } else {
			return 0;
		}
	}
	// End of the function
	
	// Start of update exercise script
	public function update_exercise($EID, $ExerciseData) {
		if ( $EID && $ExerciseData ) {
			$query = $this->db->where('EID', $EID)->update('exercises', $ExerciseData);
			if ( $query ) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of update point script
	public function update_earnpoints($EarnID, $EarnPointData) {
		if ( $EarnID && $EarnPointData ) {
			$query = $this->db->where('EarnID', $EarnID)->update('points', $EarnPointData);
			if ( $query ) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of update member script
	public function update_member($MID, $MemberData) {
		if ( !empty($MID) && !empty($MemberData) ) {
			$query = $this->db->where('MID', $MID)->update('members', $MemberData);
			if ( $query ) {
				return 1;
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of get all member script
	public function get_members() {
		$this->db->order_by("MID", "DESC");
		$query = $this->db->get("members");
		return $query->result();
	}
	// End of the function
	
	// Start of check member exist script
	public function check_member_exist($MNumber){
		$this->db->where("MNumber", $MNumber);
		$query = $this->db->get("members");
		if ( $query->num_rows() == 1 ) {
			return 1;
		} else {
			return 0;
        }
	}
	// End of the function
	
	// Start of check member exist script
	public function check_new_member_exist($MNumber){
		$this->db->where("MNumber", $MNumber);
		$query = $this->db->get("members");
		if ( $query->num_rows() == 1 ) {
			return 1;
		} else {
			return 0;
        }
	}
	// End of the function
	
	// Start of insert gift script
	public function insert_gift($GiftData) {
		if ( !empty($GiftData) ) {
			$query = $this->db->set('GftCreated', 'NOW()', FALSE)->insert('gifts', $GiftData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of get all gifts script
	public function get_gifts() {
		$this->db->order_by("GftID", "DESC");
		$query = $this->db->get("gifts");
		return $query->result();
	}
	// End of the function
	
	// Start of get all members weight script
	public function get_member_list() {
		$this->db->order_by("MID", "DESC");
		$query = $this->db->get("members");
		return $query->result();
	}
	// End of the function
	
	// Start of insert group script
	public function insert_group($GroupData) {
		if ( !empty($GroupData) ) {
			$query = $this->db->set('GCreated', 'NOW()', FALSE)->insert('groups', $GroupData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of get all groups script
	public function get_group() {
		$this->db->order_by("GID", "DESC");
		$query = $this->db->get("groups");
		return $query->result();
	}
	// End of the function
	
	// Start of get all leaders script
	public function get_leaders() {
		$query = $this->db->select('M.MID,M.MName,M.MEarnPoint,GROUP_CONCAT(G.GftName) as Giftname,G.GftPoints,G.GftDate')
						  ->from('members as M, gifts as G')
						  ->where('G.GftPoints<=M.MEarnPoint')
						  ->where('DATE(G.GftDate) > DATE(NOW())') 
						  ->order_by('M.MEarnPoint','DESC')
						  ->group_by('M.MID')->get();
						  //echo $this->db->last_query(); die;
		return $query->result();
	}
	// End of the function
	
	// Start of get all member script
	public function get_group_data($id) {
		$this->db->where("GID", $id);
		$query = $this->db->get("groups");
		return $query->result();
	}
	// End of the function
	
	// Start of get_single_exercise_data script
	public function get_single_exercise_data($id){
		$this->db->where("EID", $id);
		$query = $this->db->get("exercises");
		return $query->result();
	}
	// End of the function
	
	// Start of get_single_point_data script
	public function get_single_point_data($id){
		$this->db->where("EarnID", $id);
		$query = $this->db->get("points");
		return $query->result();
	}
	// End of the function
	
	// Start of get_single_member_data script
	public function get_single_member_data($id){
		$this->db->where("MID", $id);
		$query = $this->db->get("members");
		return $query->result();
	}
	// End of the function
	
	// Start of delete_exercise_data script
	public function delete_exercise_data($exercise_id){
		$this->db->where('EID', $exercise_id)->delete('exercises');
	}
	// End of the function
	
	// Start of get_group_members script
	public function get_group_members($GID){
		//$query = $this->db->select('*')->from('answer AS A, members AS M')->where('A.AnswerMID = M.MID AND M.MGpid = '. $GID)->group_by('MID')->get();
		$mQuery = $this->db->select('*')->from('members')->where('MGpid='.$GID)->get();
		$members = $mQuery->result();  
		
		$values = array();
		for ( $i = 0; $i < count($members); $i++ ) {
			$aQuery = $this->db->select('*')->from('answer')->where('AnswerMID='.$members[$i]->MID)->get();
			$answer = $aQuery->result();
			
			$values[$i]['member'] = $members[$i];
			$values[$i]['answer'] = $answer;
		}
		return $values;
    }
	// End of the function
	
	// Start of get member with memberid script
	public function get_id_members($id){
		$this->db->where("MID", $id);
		$query = $this->db->get("members");
		return $query->result(); 
    }
	// End of the function
	
	// Start of insert_targets script
	public function insert_targets($EID, $GID, $state, $day) {
		$this->db->where("TEID", $EID);
		$this->db->where("TGID", $GID);
		$query = $this->db->get("targets");
		if ( $query->num_rows() == 0 ) {
			$TargetsData = array(
				'TEID' => $EID,
				'TGID' => $GID,
				$day => ($state == 'true' ? '1' : '0'),
			);
			$query = $this->db->set('TCreated', 'NOW()', FALSE)->insert('targets', $TargetsData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			$TargetsData = array(
				$day => ($state == 'true' ? '1' : '0'),
			);
			$this->db->where("TEID", $EID);
			$this->db->where("TGID", $GID);
			$query = $this->db->update('targets', $TargetsData);
			if ( $query ) {
				return 1;
			} else {
				return 0;
			}
        }
	}
	// End of the function	
	
	// Start of get_group_members script
	public function get_referral($app_id){
		$this->db->where("RefAppcode", $app_id);
		$query = $this->db->get("referral");
		return $query->result(); 
    }
	// End of the function
	
	// Start of update_reffer_status script
	public function update_reffer_status($id,$user_id){
		$data = array(
               'RefStatus' => "COMPLETE"
            );
		$this->db->where("RefUserid", $user_id);
		$this->db->where("RefID",$id);
		$query = $this->db->update('referral', $data);
	}
	// End of the function
	
	// Start of get_targets_by_group script
	public function get_targets_by_group($gid){
		$this->db->where("TGID", $gid)->order_by("TEID", "DESC");
		$query = $this->db->get("targets");
		return $query->result(); 
	}
	// End of the function
	
	// Start of insert point script
	public function insert_point($PointData) {
		if ( !empty($PointData) ) {
			$query = $this->db->insert('points', $PointData);
			if ( $query ) {
				return $this->db->insert_id();
			} else {
				return 0;
			}
		} else {
			return 0;
		}			
	}
	// End of the function
	
	// Start of get all earn point script
	public function get_earn_point() {
		$this->db->order_by("EarnID", "DESC");
		$query = $this->db->get("points");
		return $query->result();
	}
	// End of the function
	
	// Start of get member with memberid script
	public function searchbox_member($data){
		$this->db->like('MName', $data, 'after');
		$query = $this->db->get("members");
		return $query->result(); 
    }
	// End of the function
	
	public function search_by_namedate($name, $date=false) {
		if ( $name ) 
			$this->db->like('MName', $name, 'after');
		if ( $date ) 
			$this->db->like('MCreated', $date, 'both');
		
		$query = $this->db->get("members");
		return $query->result(); 
	}
	// End of the function
	
	// Start of user earnpoint script
	public function get_earnpoint($id) {
		$this->db->where("EarnID", $id);
		$query = $this->db->get("points");
		return $query->result();
					
	}
	// End of the function
	
	//Start update member earnpoint script
	public function update_member_earnpoint($user_id,$userearnpoint){
		$data = array(
               'MEarnPoint' => $userearnpoint
            );
		$this->db->where("MID", $user_id);
		$query = $this->db->update('members', $data);
	}
	// End of the function
	
	//Start group Exercise Data
	public function get_group_exercise($gid){
		$this->db->select('*');
		$this->db->from('exercises');
		$this->db->join('targets', 'exercises.EID = targets.TEID');
		$this->db->where('targets.TGID', $gid);
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	//Start group_wise single exercise Data
	public function group_exercise_single_data($gid,$eid){
		$this->db->select('*');
		$this->db->from('exercises');
		$this->db->join('targets', 'exercises.EID = targets.TEID');
		$this->db->where('targets.TGID', $gid);
		$this->db->where('targets.TEID', $eid);
		$query = $this->db->get();
		return $query->result();
	}
	// End of the function
	
	//Start update_exercise_unit function
	public function update_exercise_unit($exercise_unit,$exercise_id,$exercise_group){
		$data = array(
               'TEUnit' => $exercise_unit
            );
		$this->db->where("TEID", $exercise_id);
		$this->db->where("TGID", $exercise_group);
		$query = $this->db->update('targets', $data);
		if( $query ) {
			return 1;
		} else {
			return 0;
		}
	}
	// End of the function
	
	// Start of get_answer script
	public function get_user_answer($member_id, $exercise_id) {
		$this->db->select('*');
		$this->db->from('answer');
		$this->db->where("AnswerMID",$member_id);
		$this->db->where("AnswerEID",$exercise_id);
		$query = $this->db->get();
		return $query->row();
	}
	// End of the function
	
	// Start of group question script
	public function group_question($MID) {
		$query = $this->db->select('*')->from('exercises AS E,members AS M,targets AS T')->where('T.TGid=M.MGpid and  E.EID=T.TEid and M.MID='. $MID)->get();
		return $query->result();  
	}
	// End of the function
	
	// Start of update answer status 
	public function update_answer_status($status, $MID, $EID, $GID) {
		$query = $this->db->where('AnswerMID', $MID)->where('AnswerEID', $EID)->where('AnswerGID', $GID)->update('answer', $status);
	}
	
}