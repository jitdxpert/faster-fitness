<?php if ( !defined('BASEPATH') ) exit('No direct script access allowed'); 

class Admin extends MY_Controller {
	
	// Start of Constructor
    public function __construct() {
        parent::__construct();
		
		$this->load->library('image_lib');
    }
	// End of the function
	
	// Start of Landing Page
	public function index() {
		$this->login();
	}
	// End of the function
	
	// Start of Login Page
	public function login() {
		$this->logged_in();
		
		$data['title'] = 'Admin Login - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->input->post('LoginSubmit') ) {
            $this->form_validation->set_rules('Email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('Password', 'Password', 'trim|required|min_length[6]|xss_clean');
			
			if ( $this->form_validation->run() == FALSE ) {
                $data['error'] = 'Please correct the following errors.';
            } else {
				$Email 		= $this->input->post('Email');
				$Password 	= sha1($this->input->post('Password'));
                if ( $this->admin_model->do_login($Email, $Password) == 1 ) {
					redirect(base_url('admin/members') . '/');
                } else {
                    $data['error'] = 'The email or password you entered is incorrect.';
                }
			}
		}
		$this->load->view('login', $data);
	}
	// End of the function
	
	// Start of Logout Script
	public function logout() {
		$newdata = array(
            'UserID' => '',
            'UserEmail' => '',
            'UserName' => '',
            'Loggedin' => FALSE,
        );
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect(base_url());
        return FALSE;
    }
	// End of the function
	
	// Start of Members Page
	public function members() {
		$this->not_logged_in();
		
		$data['title'] = 'Members - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->session->userdata('MUpdated') ) {
			$data['success'] = $this->session->userdata('MUpdated');
		}
		if ( $this->input->post('memberUpdate') ) {
			$MID        = $this->input->post('MID');
			$MName 		= $this->input->post('MName');
			$MEmail 	= $this->input->post('MEmail');
			$MAge 		= $this->input->post('MAge');
			$MAddress 	= $this->input->post('MAddress');
			$MemberData = array(
				'MName'		=> $MName,
				'MEmail'	=> $MEmail,
				'MAge'		=> $MAge,
				'MAddress'	=> $MAddress,
			);
			$Success = $this->admin_model->update_member($MID, $MemberData);
			if ( $Success == 1 ) {
				$data['success'] = "Member is successfully updated.";	
			} else {
				$data['error'] = 'Something went wrong, please try again!';
			}
		}
		
		if ( $this->input->post('weightUpdate') ) {
			$MID        = $this->input->post('MID');
			$Weight		= $this->input->post('Weight');
			$Success = $this->admin_model->insert_member_weight($MID, $Weight);
			if ( $Success == 1 ) {
				$MemberData = array(
					'MWeight'	=> $Weight,
				);
				$WSuccess = $this->admin_model->update_member($MID, $MemberData);
				if ( $WSuccess == 1 ) {
					$data['success'] = "Member weight is successfully updated.";
				} else {
					$data['error'] = 'Something went wrong, please try again!';
				}
			} else {
				$data['error'] = 'Something went wrong, please try again!';
			}
		}
		if ( $this->input->post('PaidStatusUpdate')){ 
			$MID        = $this->input->post('MID');
			$MExpireDate =$this->input->post('date');
			$MStatus	=$this->input->post('MStatus');
			$user_details = $this->admin_model->get_id_members($MID);
			$member_existing_point = $user_details[0]->MEarnPoint;
			$member_earn_point = $this->admin_model->get_earnpoint(7);
			$member_total_point = $member_existing_point + $member_earn_point[0]->EarnPoints;
			$MemberData = array(
					'MExpireDate'	=> $MExpireDate,
					'MStatus' 		=>$MStatus,
					'MEarnPoint'    =>$member_total_point,
			);
			$EXSuccess = $this->admin_model->update_member($MID, $MemberData);
			if ( $EXSuccess == 1 ) {
				$data['success'] = "Member status successfully updated.";
			} else {
				$data['error'] = 'Something went wrong, please try again!';
			}
		} 
		$data['members'] = $this->admin_model->get_members();
		if ( $this->input->post('search') ) {
			$name = $this->input->post('member');
			$date = $this->input->post('date');
			$data['members'] = $this->admin_model->search_by_namedate($name, $date);
		}
		$this->load->view('header', $data);
		$this->load->view('members', $data);
		$this->load->view('footer', $data);	
		$this->session->unset_userdata('MUpdated');
	}
	// End of the function
	
	// Start of Add Member Page
	public function add_member($update = FALSE) {
		$this->not_logged_in();
		
		$data['title'] = 'Add Existing Member - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $update == FALSE ) {
			$data['APPCode'] = $this->generate_code(8);
			if ( $this->input->post('MSubmit') ) {
				$this->form_validation->set_rules('MName', 		'Member Name', 		'trim|required');
				$this->form_validation->set_rules('MType', 		'Member Status', 	'trim|required');
				$this->form_validation->set_rules('MAPPCode', 	'APP Code', 		'trim|required');
				$this->form_validation->set_rules('MNumber', 	'Contact Number', 	'trim|required');
				$this->form_validation->set_rules('MEmail', 	'Email ID', 		'trim|required|valid_email');
				$this->form_validation->set_rules('MAge', 		'Age', 				'trim|required');
				$this->form_validation->set_rules('MGender', 	'Gender', 			'trim|required');
				$this->form_validation->set_rules('MWeight', 	'Weight', 			'trim|required');
				$this->form_validation->set_rules('MAddress', 	'Member Address', 	'trim|required');
				$this->form_validation->set_rules('MStatus', 	'Payment Status', 	'trim|required');
				if ( $this->form_validation->run() == FALSE ) {
					$data['error'] = 'Please correct the following errors.';
				} else { 
					$MName 		= $this->input->post('MName');
					$MType 		= $this->input->post('MType');
					$MAPPCode 	= $this->input->post('MAPPCode');
					$MNumber 	= $this->input->post('MNumber');
					$MEmail 	= $this->input->post('MEmail');
					$MAge 		= $this->input->post('MAge');
					$MGender 	= $this->input->post('MGender');
					$MWeight 	= $this->input->post('MWeight');
					$MAddress 	= $this->input->post('MAddress');
					$MStatus 	= $this->input->post('MStatus');
					if ( $this->admin_model->check_member_exist($MNumber) == 1 ) {
						$data['error'] = 'Member is already exist.';
					} else {
						$data['user_earnpoint'] = $this->admin_model->get_earnpoint(1);
						$MemberData = array(
							'MName'		=> $MName,
							'MType'		=> $MType,
							'MAPPCode'  => $MAPPCode,
							'MNumber'   => $MNumber,
							'MEmail'	=> $MEmail,
							'MAge'		=> $MAge,
							'MGender'   => $MGender,
							'MWeight'   => $MWeight,
							'MAddress'	=> $MAddress,
							'MStatus'	=> $MStatus,
							'MEarnPoint'=> $data['user_earnpoint'][0]->EarnPoints,
						);
						$Success = $this->admin_model->insert_member($MemberData);
						if ( $Success ) {
							$MLink = 'http://wisely.tv/faster-fitness/Faster-Fitness.apk';
							$TONumber = $MNumber;
							$TOMessage = "Dear $MName, your APP Code is {$MAPPCode}  .Please download the FASTER Fitness app from below link: {$MLink}";
							$Send = $this->send_sms($TONumber, $TOMessage);
							if ( $Send ) {
								$this->admin_model->insert_member_weight($Success, $MWeight);
								$this->session->set_userdata('MID', $Success);
								$this->session->set_userdata('MCreated', 'Member is successfully created. You can also update the following details.');
								redirect(base_url('admin/add-member/update') . '/');
								return FALSE;
							} else {
								$data['error'] = 'Something went wrong, please try again!';
							}
						} else {
							$data['error'] = 'Something went wrong, please try again!';
						}
					}
				}
			}
		} else {
			if ( !$this->session->userdata('MCreated') || !$this->session->userdata('MID') ) {
				redirect(base_url('admin/add-member') . '/');
				return FALSE;
			} else {
				$data['groups'] = $this->admin_model->get_group();
				$data['success'] = $this->session->userdata('MCreated');
				if ( $this->input->post('MUpdate') ) {
					$this->form_validation->set_rules('MExpireDate', 	'Membership Valid Till', 	'trim');
					$this->form_validation->set_rules('MTrialDays', 	'Free Trial Days', 			'trim|regex_match[/^[0-9,]+$/]');
					$this->form_validation->set_rules('MGpid',          'Choose member group',      'trim|required');
					if ( $this->form_validation->run() == FALSE ) {
						$data['error'] = 'Please correct the following errors.';
					} else {
						$MExpireDate 	= $this->input->post('MExpireDate');
						$MTrialDays 	= $this->input->post('MTrialDays');
						$MGpid          = $this->input->post('MGpid');
						$MemberData = array(
							'MExpireDate'	=> $MExpireDate,
							'MTrialDays'	=> $MTrialDays,
							'MGpid'         => $MGpid,
						);
						$MID = $this->session->userdata('MID');
						$Success = $this->admin_model->update_member($MID, $MemberData);
						if ( $Success == 1 ) {
							$this->session->unset_userdata('MCreated');
							$this->session->unset_userdata('MID');
							$this->session->set_userdata('MUpdated', 'Member record is successfully updated.');
							redirect(base_url('admin/members') . '/');
							return FALSE;
						} else {
							$data['error'] = 'Something went wrong, please try again!';
						}
					}
				}
			}
		}
		
		$this->load->view('header', $data);
		$this->load->view('add-member', $data);
		$this->load->view('footer', $data);	
	}
	// End of the function
    
	// Start of the Add Exercise Page
	public function add_exercise() {
		$this->not_logged_in();
		
		$data['title'] = 'Add Exercise - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->input->post('ESubmit') ) {
			$this->form_validation->set_rules('EName', 'Exercise Name', 'trim|required');
			$this->form_validation->set_rules('EUnitType', 'Exercise Unit Type', 'trim|required|alpha');
			$this->form_validation->set_rules('EUnit', 'Exercise Unit', 'trim|required|numeric');
			$this->form_validation->set_rules('ENotes', 'Exercise Notes', 'trim|required');
			$this->form_validation->set_rules('EQuestion', 'Exercise Question', 'trim|required');
			
			if ( $this->form_validation->run() == FALSE ) {
                $data['error'] = 'Please correct the following errors.';
            } else {
				$EName = $this->input->post('EName');
				$EUnitType = $this->input->post('EUnitType');
				$EUnit = $this->input->post('EUnit');
				$ENotes = $this->input->post('ENotes');	
				$EQuestion = $this->input->post('EQuestion');	
				
				$ExerciseData = array(
					'EName'		=> $EName,
					'EUnitType'	=> $EUnitType,
					'EUnit'		=> $EUnit,
					'ENotes'	=> $ENotes,
					'EQuestion'	=> $EQuestion,
				);
				$Success = $this->admin_model->insert_exercise($ExerciseData);
				if ( $Success ) {
					 $data['success'] = "Exercise is successfully created.";
				} else {
					$data['error'] = 'Something went wrong, please try again!';
				}
			}
		}
		
		$this->load->view('header', $data);
		$this->load->view('add-exercise', $data);
		$this->load->view('footer', $data);
	}	
	// End of the function
	
	// Start of the Exercises Page
	public function exercises() {
		$this->not_logged_in();
		
		$data['title'] = 'Exercises - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->session->userdata('ExCreated') ) {
			$data['success'] = $this->session->userdata('ExCreated');
		}

		if ( $this->input->post('EUpdate') ) {
			$this->form_validation->set_rules('EUnitType', 'Exercise Unit Type', 'trim|required');
			$this->form_validation->set_rules('EUnit', 'Exercise Unit', 'trim|required');
			$this->form_validation->set_rules('EQuestion', 'Exercise Question', 'trim|required');
			if ( $this->form_validation->run() == FALSE ) {
				$data['error'] = 'Please correct the following errors.';
			} else {
				$EUnitType = $this->input->post('EUnitType');
				$EUnit = $this->input->post('EUnit');
				$EQuestion = $this->input->post('EQuestion');
				$EID = $this->input->post('EID');
				$ExerciseData = array(
					'EUnitType'	=> $EUnitType,
					'EUnit'	=> $EUnit,
					'EQuestion'	=> $EQuestion,
				); 
				$Success = $this->admin_model->update_exercise($EID, $ExerciseData);
				if ( $Success == 1 ) {
					$data['success'] = "Exercise is successfully updated.";
				} else {
					$data['error'] = 'Something went wrong, please try again!';
				}
			}
		}
		
		$data['exercises'] = $this->admin_model->get_exercises();
		
		$this->load->view('header', $data);
		$this->load->view('exercises', $data);
		$this->load->view('footer', $data);
		$this->session->unset_userdata('ExCreated');
	}
    // End of the function
	
	// Start of Gifts Page
	public function gifts() {
		$this->not_logged_in();
		
		$data['title'] = 'Gifts - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		$data['gifts'] = $this->admin_model->get_gifts();
		
		$this->load->view('header', $data);
		$this->load->view('gifts', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the Add Gift Page
    public function add_gift() {
	    $this->not_logged_in();
		
		$data['title'] = 'Add Gift - Faster Fitness';
		$data['error'] = ''; $data['success'] = ''; 
			if ( $this->input->post('GSave') ) {
			$this->form_validation->set_rules('GName', 'Gift Name', 'trim|required');
			$this->form_validation->set_rules('GNotes', 'Gift Notes', 'trim|required');
			$this->form_validation->set_rules('GPoints', 'Gift Points', 'trim|required');
			$this->form_validation->set_rules('GDate', 'Gift Date', 'trim|required');
			if ( $this->form_validation->run() == FALSE ) {
				$data['error'] = 'Please correct the following errors.';
            } else {
				$GName = $this->input->post('GName');
				$GNotes = $this->input->post('GNotes');
				$GPoints = $this->input->post('GPoints');
				$GDate = $this->input->post('GDate');
				 if (isset($_FILES['GftPicName']) && !empty($_FILES['GftPicName']['name'])) {
				    $file_name = time() . rand(1, 988);
					$config['upload_path'] = FCPATH . 'assets/gifts/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
					$config['max_size'] = '1024';
					$config['max_width'] = '0';
					$config['max_height'] = '0';
					$config['file_name'] = $file_name;
					$config['overwrite'] = TRUE;
					$this->upload->initialize($config);
					$this->load->library('upload', $config);
					
					if ($this->upload->do_upload('GftPicName')) {
						$upload_data = $this->upload->data();
						$ext = pathinfo($upload_data['orig_name'], PATHINFO_EXTENSION);
						$picture = base_url('assets/gifts/' . $upload_data['orig_name']);
						
						// Create 50x50 thumbnail
						$crop_config_1['image_library'] = 'gd2';
						$crop_config_1['source_image'] = $upload_data["full_path"];
						$crop_config_1['new_image'] = $upload_data["file_path"] .'50x50_' .  $file_name . '.' . $ext;
						$crop_config_1['quality'] = "100%";
						$crop_config_1['maintain_ratio'] = FALSE;
						$crop_config_1['create_thumb'] = TRUE;
						$crop_config_1['thumb_marker'] = '';
						$crop_config_1['width'] = 50;
						$crop_config_1['height'] = 50;

						$this->image_lib->initialize($crop_config_1);
						$this->image_lib->resize();
						$this->image_lib->clear();
					
						$GiftData = array(
							'GftName'		=> $GName,
							'GftNotes'	=> $GNotes,
							'GftPoints'		=> $GPoints,
							'GftDate'	=> $GDate,
							'GftPicName'           =>$upload_data['orig_name'],
						);
						$Success = $this->admin_model->insert_gift($GiftData);
						if ( $Success ) {
							 $data['success'] = "Gift is successfully created.";
						} else {
							$data['error'] = 'Something went wrong, please try again!';
						}
					} else {
						$data['error'] = $this->upload->display_errors();
					}
				}
			}
		}
		
		$this->load->view('header', $data);
		$this->load->view('add-gift', $data);
		$this->load->view('footer', $data);
    }
    // End of the function	
	
	// Start of the Add Group Page
    public function add_group() {
		$this->not_logged_in();
		
		$data['title'] = 'Add Group - Faster Fitness';
		$data['error'] = ''; $data['success'] = ''; 
		if ( $this->input->post('GSubmit') ) {
			$this->form_validation->set_rules('GrName', 'Group Name', 'trim|required');
			$this->form_validation->set_rules('GrText', 'Group Description', 'trim|required');
			if ( $this->form_validation->run() == FALSE ) {
				$data['error'] = 'Please correct the following errors.';
            } else {
				$GrName = $this->input->post('GrName');
				$GrText = $this->input->post('GrText');
				if (isset($_FILES['GPicName']) && !empty($_FILES['GPicName']['name'])) {
				    $file_name = time() . rand(1, 988);
					$config['upload_path'] = FCPATH . 'assets/groups/';
					$config['allowed_types'] = 'gif|jpg|jpeg|png|bmp';
					$config['max_size'] = '1024';
					$config['max_width'] = '0';
					$config['max_height'] = '0';
					$config['file_name'] = $file_name;
					$config['overwrite'] = TRUE;
					$this->upload->initialize($config);
					$this->load->library('upload', $config);
					
					if ($this->upload->do_upload('GPicName')) {
						$upload_data = $this->upload->data();
						$ext = pathinfo($upload_data['orig_name'], PATHINFO_EXTENSION);
						$picture = base_url('assets/groups/' . $upload_data['orig_name']);
						
						// Create 50x50 thumbnail
						$crop_config_1['image_library'] = 'gd2';
						$crop_config_1['source_image'] = $upload_data["full_path"];
						$crop_config_1['new_image'] = $upload_data["file_path"] .'50x50_' .  $file_name . '.' . $ext;
						$crop_config_1['quality'] = "100%";
						$crop_config_1['maintain_ratio'] = FALSE;
						$crop_config_1['create_thumb'] = TRUE;
						$crop_config_1['thumb_marker'] = '';
						$crop_config_1['width'] = 50;
						$crop_config_1['height'] = 50;
						
						$this->image_lib->initialize($crop_config_1);
						$this->image_lib->resize();
						$this->image_lib->clear();
						
						// Create 600x300 thumbnail
						$crop_config_2['image_library'] = 'gd2';
						$crop_config_2['source_image'] = $upload_data["full_path"];
						$crop_config_2['new_image'] = $upload_data["file_path"] .'250x150_' .  $file_name . '.' . $ext;
						$crop_config_2['quality'] = "100%";
						$crop_config_2['maintain_ratio'] = FALSE;
						$crop_config_2['create_thumb'] = TRUE;
						$crop_config_2['thumb_marker'] = '';
						$crop_config_2['width'] = 250;
						$crop_config_2['height'] = 150;
						
						$this->image_lib->initialize($crop_config_2);
						$this->image_lib->resize();
						$this->image_lib->clear();
						
						$GroupData = array(
							'GName'		        => $GrName,
							'GDescription'	    => $GrText,
							'GPicName'           =>$upload_data['orig_name'],
						);
						$Success = $this->admin_model->insert_group($GroupData);
						if ( $Success ) {
							 $data['success'] = "Group is successfully created.";
						} else {
							$data['error'] = 'Something went wrong, please try again!';
						}
					} else {
						$data['error'] = $this->upload->display_errors();
					}
				}
			}
		}
		
		$this->load->view('header', $data);
		$this->load->view('add-group', $data);
		$this->load->view('footer', $data);
	}
    // End of the function 
    
	// Start of the Groups Page
    public function groups() {
		$this->not_logged_in();
		
		$data['title'] = 'Groups - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		$data['groups'] = $this->admin_model->get_group();
		
		$this->load->view('header', $data);
		$this->load->view('groups', $data);
		$this->load->view('footer', $data);
	}
    // End of the function 	
	
	// Start of the Leaderboard Page
	public function leaderboard() {
		$this->not_logged_in();
		
		$data['title'] = 'Leaderboard - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
         		
	
		$data['leaderboard'] = $this->admin_model->get_leaders();
		
		
		$this->load->view('header', $data);
		$this->load->view('leaderboard', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the Targets Page
	public function targets($gid) {
		$this->not_logged_in();
		
		$data['title'] = 'Targets - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		
		$data['gpdata'] = $this->admin_model->get_group_data($gid);
		$data['exdata'] = $this->admin_model->get_exercises();	
		
		$this->load->view('header', $data);
		$this->load->view('targets', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the Performance Page
	public function performance($id) {
		$this->not_logged_in();
		
		$data['title'] = 'Performance - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		
		if ( $this->input->post('AnsApprove') ) {
			$AnswerMID = $this->input->post('AnswerMID');
			$AnswerGID = $this->input->post('AnswerGID');
			
			$AnswerEID = $this->input->post('AnswerEID');
			if ( count($AnswerEID) > 0 ) {
				for ( $i = 0; $i < count($AnswerEID); $i++ ) {
					$status = array('AnswerStatus' => 'Approve');
					$this->admin_model->update_answer_status($status, $AnswerMID, $AnswerEID[$i], $AnswerGID);
				}
				$user_details = $this->admin_model->get_id_members($AnswerMID);
				$member_existing_point = $user_details[0]->MEarnPoint;
				$member_earn_point = $this->admin_model->get_earnpoint(10);
				$member_total_point = $member_existing_point + $member_earn_point[0]->EarnPoints;
				$MemberData = array(
					'MEarnPoint'    =>$member_total_point,
				);
				$EXSuccess = $this->admin_model->update_member($AnswerMID, $MemberData);
				if ( $EXSuccess == 1 ) {
					$data['success'] = 'Answer Status successfully approved.';
				} else {
					$data['error'] = 'Something went wrong, please try again!';
				}
			} else {
				$data['error'] = 'Please check at least one answer to approve.';
			}
		}
		
		if ( $this->input->post('AnsReject') ) {
			$AnswerMID = $this->input->post('AnswerMID');
			$AnswerGID = $this->input->post('AnswerGID');
			
			$AnswerEID = $this->input->post('AnswerEID');
			if ( count($AnswerEID) > 0 ) {
				for ( $i = 0; $i < count($AnswerEID); $i++ ) {
					$status = array('AnswerStatus' => 'Reject');
					$this->admin_model->update_answer_status($status, $AnswerMID, $AnswerEID[$i], $AnswerGID);
				}
				$data['success'] = 'Answer Status successfully rejected.';
			} else {
				$data['error'] = 'Please check at least one answer to reject.';
			}
		}
		
		$data['gpmember'] = $this->admin_model->get_group_members($id);
		
		$this->load->view('header', $data);
		$this->load->view('performance', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the get_single_exercise Page
	public function get_single_exercise() {
		$this->not_logged_in();
		
		$html = '';
		$exercise_id = $_POST['exercise_id'];
		$data['exdata'] = $this->admin_model->get_single_exercise_data($exercise_id);
		
		foreach($data['exdata'] as $exdata){
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="exerciseModalLabel">Exercise: <strong>'.$exdata->EName.'</strong></h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form action="' . base_url('admin/exercises') . '/" method="post" id="ajaxexercise" name="ajaxexercise" class="form-horizontal">';
			$html .= '<div class="form-group">';
			$html .= '<label for="unit" class="col-sm-4 control-label">Exercise Unit</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="EUnit" name="EUnit" class="form-control" value="'.$exdata->EUnit.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="unit" class="col-sm-4 control-label">Unit Type</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" class="form-control" id="EUnitType" name="EUnitType" value="'.$exdata->EUnitType.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="unit" class="col-sm-4 control-label">Exercise Question</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" class="form-control" id="EQuestion" name="EQuestion" value="'.$exdata->EQuestion.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="submit" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="EID" value="'.$exdata->EID.'" />';
			$html .= '<button name="EUpdate" type="submit" id="EUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		echo $html;
	}
	// End of the function
	
	// Start of the get_single_point Page
	public function get_single_point() {
		$this->not_logged_in();
		
		$html = '';
		$point_id = $_POST['earnpoint_id'];
		$data['pointdata'] = $this->admin_model->get_single_point_data($point_id);
		
		foreach($data['pointdata'] as $pointdata){
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="pointModalLabel">Earn Point: <strong>' . $pointdata->EarnTopic . '</strong></h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form action="' . base_url('admin/earn-point') . '/" method="post" id="ajaxpoint" name="ajaxpoint" class="form-horizontal">';
			$html .= '<div class="form-group">';
			$html .= '<label for="earn_topic" class="col-sm-4 control-label">Earn Point Topic</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="EarnTopic" readonly="readonly" name="EarnTopic" class="form-control" value="'.$pointdata->EarnTopic.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="earn_points" class="col-sm-4 control-label">Earn Points</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" class="form-control" id="EarnPoints" name="EarnPoints" value="'.$pointdata->EarnPoints.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="earn_points" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="EarnID" value="'.$pointdata->EarnID.'" />';
			$html .= '<button name="EarnUpdate" type="submit" id="EarnUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		echo $html;
	}
	// End of the function	
	
	// Start of the edit_single_member Page
	public function edit_single_member() {
		$this->not_logged_in();
		
		$html = '';
		$emember_id = $_POST['emember_id'];
		$data['memberdata'] = $this->admin_model->get_single_member_data($emember_id);
		
		foreach($data['memberdata'] as $memberdata){
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="memberModalLabel">Edit Member: <strong>' . $memberdata->MName . '</strong></h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form action="' . base_url('admin/members') . '/" method="post" id="ajaxmember" name="ajaxmember" class="form-horizontal">';
			$html .= '<div class="form-group">';
			$html .= '<label for="name" class="col-sm-4 control-label">Name</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="MName" name="MName" class="form-control" value="'.$memberdata->MName.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="age" class="col-sm-4 control-label">Age</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" class="form-control" id="MAge" name="MAge" value="'.$memberdata->MAge.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="address" class="col-sm-4 control-label">Address</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" class="form-control" id="MAddress" name="MAddress" value="'.$memberdata->MAddress.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="email" class="col-sm-4 control-label">Email ID</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="MEmail" name="MEmail" class="form-control" value="'.$memberdata->MEmail.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="submit" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="MID" value="'.$memberdata->MID.'" />';
			$html .= '<button name="memberUpdate" type="submit" id="memberUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		echo $html;
	}
	// End of the function
	
	public function edit_payment_status(){
		$this->not_logged_in();
		
		$member_id = $_POST['member_id'];
		$data['memberdata'] = $this->admin_model->get_single_member_data($member_id);
		    $html='';
				
		foreach($data['memberdata'] as $memberdata){
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="paymentModalLabel"> Member Name: <strong>' . $memberdata->MName . '</strong></h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<div class="form-group">';
			$html .= '<label for="ExpairyDay" class="col-sm-4 control-label">Next Expiry Days:</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="nextexpirydays" class="form-control" style="float:left;width:60%;padding:6px;"/>';
			$html .= '<button class="btn green small" id="get_days" style="padding:5px 12px;">';
			$html .= '<i class="fa fa-search"></i>';
			$html .= '<i class="fa fa-check text-success hide"></i>';
			$html .= '<i class="fa fa-times text-danger hide"></i>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<form action="' . base_url('admin/members') . '/" method="post" id="ajaxmember" name="ajaxmember" class="form-horizontal">';
			$html .= '<div class="form-group hide expiry">';
			$html .= '<label for="email" class="col-sm-4 control-label">Next Expiry Date:</label>';
			$html .= '<div class="col-sm-8 ">';
			$html .= '<input type="text" id="date_expire" name="date" class="form-control"  readonly="true" data-date-format="yyyy-mm-dd"  value="'.$memberdata->MExpireDate.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="payment" class="col-sm-4 control-label">Payment Status</label>';
			$html .= '<div class="col-sm-8" style="padding-top:8px;">';
			if ( $memberdata->MStatus == "Paid" ) {
				$html .= '<input type="radio" name="MStatus" value="Paid" checked="checked" /> Paid&nbsp;&nbsp;&nbsp;';
				$html .= '<input type="radio" name="MStatus" value="Unpaid" /> Unpaid';
			} else {
				$html .= '<input type="radio" name="MStatus" value="Paid" /> Paid&nbsp;&nbsp;&nbsp;';
				$html .= '<input type="radio" name="MStatus" value="Unpaid" checked="checked" /> Unpaid';
			}
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="submit" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="MID" id="MID" value="'.$memberdata->MID.'" />';
			$html .= '<button name="PaidStatusUpdate" type="submit" id="PaidStatusUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}	
			echo $html;
	}
	
	// End of the function
	
	public function get_days_todate(){
		$this->not_logged_in();
		$exday = $_POST['exdate'];
		$MID = $_POST['MID'];
		$data['memberdata'] = $this->admin_model->get_single_member_data($MID);
		if(isset($exday)){
			echo $desire_date = date('d-m-Y', strtotime(date("Y-m-d"). ' + '.$exday.' days'));
		} else {
			echo "No";
		}	
	}
	
	// Start of the edit_member_weight Page
	public function edit_member_weight() {
		$this->not_logged_in();
		
		$html = '';
		$emember_id = $_POST['emember_id'];
		$data['memberdata'] = $this->admin_model->get_single_member_data($emember_id);
		
		foreach($data['memberdata'] as $memberdata){
			$weights = $this->admin_model->get_member_weights($memberdata->MID);
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="weightModalLabel">Member: ' . $memberdata->MName . '</h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form action="' . base_url('admin/members') . '/" method="post" id="ajaxmember" name="ajaxmember" class="form-horizontal">';
			$html .= '<table class="table table-bordered">';
			$html .= '<thead>';
			$html .= '<tr>';
			$html .= '<th>Month</th>';
			$html .= '<th>Weights (kgs)</th>';
			$html .= '</tr>';
			$html .= '</thead>';
			$html .= '<tbody>';
			if ( $weights ) {
				foreach ( $weights as $weight ) {
					$html .= '<tr>';
					$html .= '<td valign="middle">' . date('F, Y', strtotime($weight->WMonthYear)) . '</td>';
					if ( date('F, Y') == date('F, Y', strtotime($weight->WMonthYear)) ) {
						$html .= '<td><input type="text" id="Weight" name="Weight" value="' . $weight->Weight . '" class="form-control" /></td>';
					} else {
						$html .= '<td>' . $weight->Weight . '</td>';
					}
					$html .= '</tr>';
					$last_month = date('F, Y', strtotime($weight->WMonthYear));
				}
				if ( date('F, Y') == date('F, Y', strtotime('+1 month', strtotime($weight->WMonthYear))) ) {
					$html .= '<tr>';
					$html .= '<td>' . date('F, Y', strtotime('+1 month', strtotime($weight->WMonthYear))) . '</td>';
					$html .= '<td><input type="text" id="Weight" name="Weight" class="form-control" /></td>';
					$html .= '</tr>';
				}				
			} else {
				$html .= '<tr>';
				$html .= '<td>' . date('F, Y') . '</td>';
				$html .= '<td><input type="text" id="Weight" name="Weight" class="form-control" /></td>';
				$html .= '</tr>';
			}
			$html .= '</tbody>';
			$html .= '</table>';
			$html .= '<div class="form-group">';
			$html .= '<label for="submit" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="MID" value="'.$memberdata->MID.'" />';
			$html .= '<button name="weightUpdate" type="submit" id="weightUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		echo $html;
	}
	// End of the function

	
	// Start of the delete exercise script
	public function delete_exercise($exercise_id){
		$this->not_logged_in();
		
		$this->admin_model->delete_exercise_data($exercise_id);
		redirect (base_url('admin/exercises').'/');
	}
	// End of the function	
	
	// Start of the save targets scripts
	public function save_targets() {
		$this->not_logged_in();
		
		$EID = $_POST['EID'];
		$GID = $_POST['GID'];
		$state = $_POST['state'];
		$day = $_POST['day'];
		
		$success = $this->admin_model->insert_targets($EID, $GID, $state, $day);
		echo $success;
	}
	// End of the function
	
	//start of new member script
	public function new_member() {
		$this->not_logged_in();
		
		$data['title'] = 'Add New Member - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		$data['groups'] = $this->admin_model->get_group();
		if ( $this->input->post('MSubmit') ) {
            $this->form_validation->set_rules('MName', 		'Member Name', 		'trim|required');
			$this->form_validation->set_rules('MType', 		'Member Status', 	'trim|required');
			$this->form_validation->set_rules('newAddID', 	'APP Code', 		'trim|required');
			$this->form_validation->set_rules('MNumber', 	'Contact Number', 	'trim|required');
			$this->form_validation->set_rules('MEmail', 	'Email ID', 		'trim|required|valid_email');
			$this->form_validation->set_rules('MAge', 		'Age', 				'trim|required');
			$this->form_validation->set_rules('MGender', 	'Gender', 			'trim|required');
			$this->form_validation->set_rules('MWeight', 	'Weight', 			'trim|required');
			$this->form_validation->set_rules('MAddress', 	'Member Address', 	'trim|required');
			$this->form_validation->set_rules('MStatus', 	'Payment Status', 	'trim|required');
            $this->form_validation->set_rules('MTrialDays', 'Free Trial Days', 			'trim|regex_match[/^[0-9,]+$/]');
			$this->form_validation->set_rules('MGpid',      'Choose member group',      'trim|required');

            if ( $this->form_validation->run() == FALSE ) {
					$data['error'] = 'Please correct the following errors.';
				} else { 
					$MName 			= $this->input->post('MName');
					$MType 			= $this->input->post('MType');
					$MAPPCode 		= $this->input->post('newAddID');
					$MNumber		= $this->input->post('MNumber');
					$MEmail 		= $this->input->post('MEmail');
					$MAge 			= $this->input->post('MAge');
					$MGender 		= $this->input->post('MGender');
					$MWeight 		= $this->input->post('MWeight');
					$MAddress 		= $this->input->post('MAddress');
					$MStatus 		= $this->input->post('MStatus');
                    $MTrialDays 	= $this->input->post('MTrialDays');
                    $MGpid 	        = $this->input->post('MGpid');
				if($this->admin_model->check_new_member_exist($MNumber) == 1){
					$data['error'] = 'Member is already exist.';
				} else {
					$data['user_earnpoint'] = $this->admin_model->get_earnpoint(2);
					$MemberData = array(
						'MName'		=> $MName,
						'MType'		=> $MType,
						'MAPPCode'  => $MAPPCode,
						'MNumber'   => $MNumber,
						'MEmail'	=> $MEmail,
						'MAge'		=> $MAge,
						'MGender'   => $MGender,
						'MWeight'   => $MWeight,
						'MAddress'	=> $MAddress,
						'MStatus'	=> $MStatus,
						'MGpid'     => $MGpid,
						'MTrialDays'=> $MTrialDays,
						'MEarnPoint'=> $data['user_earnpoint'][0]->EarnPoints,
					);
					$earn_point = $data['user_earnpoint'][0]->EarnPoints;
					$Success = $this->admin_model->insert_new_member($MemberData);
					$TONumber = $MNumber;
					$TOMessage = "Congratulations!!! Dear $MName, you get $MTrialDays days free trail in Gym and $earn_point points. Use this $MAPPCode to activate your app.";
					$Send = $this->send_sms($TONumber, $TOMessage);
                    if ( $Success ) {
						$this->admin_model->insert_member_weight($Success, $MWeight);
						$reffer_user_id = $this->input->post('RefferUserID');
						$refferal_id = $this->input->post('RequestID');
						$this->admin_model->update_reffer_status($refferal_id,$reffer_user_id);
						$user_details = $this->admin_model->get_id_members($reffer_user_id);
						$member_existing_point = $user_details[0]->MEarnPoint;
						if($this->input->post('RefStatus') == "SMS"){
							$member_earn_point = $this->admin_model->get_earnpoint(3);
						} else if($this->input->post('RefStatus') == "Facebook"){
							$member_earn_point = $this->admin_model->get_earnpoint(5);
						} else{
							$member_earn_point = $this->admin_model->get_earnpoint(6);
						}
						$member_total_point = $member_existing_point + $member_earn_point[0]->EarnPoints;
						$this->admin_model->update_member_earnpoint($reffer_user_id,$member_total_point);
						$OLDUserName = $user_details[0]->MName;
						$OLDUserNumber = $user_details[0]->MNumber;
						$OLDUserEarnPoint = $member_earn_point[0]->EarnPoints;
						$OLDUserMessage = "Congratulations!!! Dear $OLDUserName, you earn $OLDUserEarnPoint points as a referral for $MName";
						$Send = $this->send_sms($OLDUserNumber, $OLDUserMessage);
						$data['success'] = 'New Member successfully updated';	
					} else {
						$data['error'] = 'Something went wrong, please try again!';	
					}
				}	
			}					
		}
		$this->load->view('header', $data);
		$this->load->view('new-member', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	//Referral Data details with app_id function
	public function referral_code_details(){
		$this->not_logged_in();
		
		$app_id = $_POST['app_code'];
		$data['referral_data'] = $this->admin_model->get_referral($app_id);	
		if(!empty($data['referral_data'])){
			$data['members'] = $this->admin_model->get_id_members($data['referral_data'][0]->RefUserid);
			if($data['referral_data'][0]->RefType == "SMS"){
				if(!empty($data['members'])){
					echo $data['members'][0]->MName."|".$data['members'][0]->MID."|".$data['referral_data'][0]->RefID."|".$data['referral_data'][0]->RefType."|".$app_id;
				} else {
					echo "No";
				}
			} else {
				$app_id =$this->generate_code(8);
				if(!empty($data['members'])){
					echo $data['members'][0]->MName."|".$data['members'][0]->MID."|".$data['referral_data'][0]->RefID."|".$data['referral_data'][0]->RefType."|".$app_id;
				} else {
					echo "No";
				}
			}
		} else {
			echo "No";
		}
	}
	// End of the function
	
	//Referral Data details with app_id function
	public function search_member_details(){
		$this->not_logged_in();
		
		$q = $_REQUEST['q'];
		$results = $this->admin_model->searchbox_member($q);
		foreach( $results as $result ) {
			echo $result->MName . "\n";
		}		
	}
	// End of the function
	
	// Start of the get-targets-exercise function
	public function get_targets_exercise() {
		$this->not_logged_in();
		
		$EID = $_POST['EID'];
		$GID = $_POST['GID'];
		$day = $_POST['day'];
		
		$targets = $this->admin_model->get_targets_by_day($EID, $GID, $day);	
		if ( $targets == 0 ) {
			echo 0;
		} else {
			echo $targets[0]->$day;
		}
	}
	// End of the function
	
	// Start of the Add Exercise Page
	public function add_earn_point() {
		$this->not_logged_in();
		
		$data['title'] = 'Add Earn Point - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->input->post('PSave') ) {
            $this->form_validation->set_rules('PTopic', 'Point Topic', 'trim|required');
			$this->form_validation->set_rules('PEarn', 	'Points Earn', 'trim|required|numeric');
			if ( $this->form_validation->run() == FALSE ) {
				$data['error'] = 'Please correct the following errors.';
            } else {
				$PTopic = $this->input->post('PTopic');
				$PEarn = $this->input->post('PEarn');
				$PointData = array(
					'EarnTopic'	    => $PTopic,
					'EarnPoints'	=> $PEarn,
				); 
				$Success = $this->admin_model->insert_point($PointData);
				if ( $Success ) {
					$data['success'] = "Point is successfully Added.";
				} else {
					$data['error'] = 'Something went wrong, please try again!';
		        }
			}
		}
		$this->load->view('header', $data);
		$this->load->view('add-earn-point', $data);
		$this->load->view('footer', $data);
	}
	// End of the function
	
	// Start of the earn_point Page
	public function earn_point() {
		$this->not_logged_in();
		
		$data['title'] = 'Earn Point - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->input->post('EarnUpdate') ) {
			$this->form_validation->set_rules('EarnTopic', 'Earn Point Topic', 'trim|required');
			$this->form_validation->set_rules('EarnPoints', 'Earn Point', 'trim|required');
			if ( $this->form_validation->run() == FALSE ) {
				$data['error'] = 'Please correct the following errors.';
			} else {
				$EarnTopic = $this->input->post('EarnTopic');
				$EarnPoints = $this->input->post('EarnPoints');
				$EarnID = $this->input->post('EarnID');
				$EarnPointData = array(
					'EarnTopic'	=> $EarnTopic,
					'EarnPoints'	=> $EarnPoints,
				); 
				$Success = $this->admin_model->update_earnpoints($EarnID, $EarnPointData);
				if ( $Success == 1 ) {
					$data['success'] = "Earn point successfully updated.";
				} else {
					$data['error'] = 'Something went wrong, please try again!';
				}
			}
		}
		$data['earnpoint'] = $this->admin_model->get_earn_point();
		
		$this->load->view('header', $data);
		$this->load->view('earn-point', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the group_exercise Page
	public function group_exercise($gid) {
		$this->not_logged_in();
		
		$data['title'] = 'Group Exercise - Faster Fitness';
		$data['error'] = ''; $data['success'] = '';
		if ( $this->input->post('groupexerciseUpdate') ) {
			$exercise_unit = $this->input->post('TEUnit');
			$exercise_id = $this->input->post('TEID');
			$exercise_group = $this->input->post('TGID');
			$Success = $this->admin_model->update_exercise_unit($exercise_unit,$exercise_id,$exercise_group);
			if ( $Success == 1 ) {
				$data['success'] = "Exercise Unit Successfully Updated.";	
			} else {
				$data['error'] = 'Something went wrong, please try again!';
			}	
		}
		$data['group_exercise'] = $this->admin_model->get_group_exercise($gid);
		$this->load->view('header', $data);
		$this->load->view('group_exercise', $data);
		$this->load->view('footer', $data);
	}
    // End of the function
	
	// Start of the edit_group_exercise Page
	public function edit_group_exercise(){
		  $html = '';
		  $exercise_id = $_POST['exercise_id'];
		  $group_id = $_POST['group_id'];
		  $data['groupexercisedata'] = $this->admin_model->group_exercise_single_data($group_id,$exercise_id);
		  foreach ( $data['groupexercisedata'] as $memberdata ) {
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="groupexerciseModalLabel">Edit Exercise Unit: ' . $memberdata->EName . '</h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form action="'.base_url('admin/group-exercise/'.$group_id.'').'/" method="post" id="ajaxgroupexercise" name="ajaxgroupexercise" class="form-horizontal">';
			$html .= '<div class="form-group">';
			$html .= '<label for="exercise_name" class="col-sm-4 control-label">Exercise Name</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" readonly id="EName" name="EName" class="form-control" value="'.$memberdata->EName.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="exercise_unit" class="col-sm-4 control-label">Exercise Unit</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="text" id="TEUnit" name="TEUnit" class="form-control" value="'.$memberdata->TEUnit.'" />';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '<div class="form-group">';
			$html .= '<label for="submit" class="col-sm-4 control-label">&nbsp;</label>';
			$html .= '<div class="col-sm-8">';
			$html .= '<input type="hidden" name="TEID" value="'.$exercise_id.'" />';
			$html .= '<input type="hidden" name="TGID" value="'.$group_id.'" />';
			$html .= '<button name="groupexerciseUpdate" type="submit" id="groupexerciseUpdate" value="true" class="btn green small">';
			$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;UPDATE&nbsp;&nbsp;';
			$html .= '</button>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}
		echo $html;
	}
	// End of the function
	
	//start of approve_performance script
	public function approve_performance(){
		$this->not_logged_in();
		
		$member_id = $_POST['member_id'];
		$data['memberdata'] = $this->admin_model->get_single_member_data($member_id);
		$data['qdata'] = $this->admin_model->group_question($member_id);
		$html = '';
		
		foreach($data['memberdata'] as $memberdata){
			$html .= '<div class="modal-dialog">';
			$html .= '<div class="modal-content">';
			$html .= '<div class="modal-header">';
			$html .= '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>';
			$html .= '<h4 class="modal-title" id="PerformanceModal">Member: <strong>' . $memberdata->MName . '</strong></h4>';
			$html .= '</div>';
			$html .= '<div class="modal-body">';
			$html .= '<form method="post" action="">';
			$html .= '<input type="hidden" name="AnswerMID" value="' . $memberdata->MID . '" />';
			$html .= '<input type="hidden" name="AnswerGID" value="' . $memberdata->MGpid . '" />';
			$html .= '<div class="row">';
			foreach($data['qdata'] as $qdata){
				$html .= '<div class="col-lg-12">';
				$html .= '<h4 class="modal-title" id="PerformanceModal">';
				$html .= '<strong>' . $qdata->EQuestion . '</strong></h4>';
				$data['answer_details'] = $this->admin_model->get_user_answer($memberdata->MID, $qdata->EID);
				$statuses = array();
				if ( isset($data['answer_details']->AnswerText) ) {
					$html .= '<h4 class="modal-title" id="PerformanceModal">';
					if ( $data['answer_details']->AnswerStatus != 'Approve' ) {
						$html .= '<input checked type="checkbox" name="AnswerEID[]" class="pull-left" value="' . $qdata->EID . '" />&nbsp;&nbsp;';
					}
					$html .= 'Answer: <strong>' . $data['answer_details']->AnswerText . '</strong><small class="pull-right">';
					if ($data['answer_details']->AnswerStatus == 'Approve') {
						$html .= 'Approved';
					} elseif ($data['answer_details']->AnswerStatus == 'Reject') {
						$html .= 'Rejected';
					} else {
						$html .= $data['answer_details']->AnswerStatus;
					}
					$html .= '</small></h4>';
					$statuses[] = $data['answer_details']->AnswerStatus;
				} else {
					$html .= '<h4 class="modal-title" id="PerformanceModal">';
					$html .= '<input disabled="disabled" type="checkbox" name="AnswerEID[]" class="pull-left" value="' . $qdata->EID . '" />&nbsp;&nbsp;';
					$html .= 'Answer: <strong>Not answered yet.</strong></h4>';
					$statuses[] = 'No Response';
				}
				$html .= '<hr />';
				$html .= '</div>';				
			}
			if (count(array_unique($statuses)) === 1 && end($statuses) !== 'Approve') {
				$html .= '<div class="col-lg-6 text-right">';
				$html .= '<button class="btn green small" value="true" id="AnsApprove" type="submit" name="AnsApprove">';
				$html .= '<i class="fa fa-check"></i>&nbsp;&nbsp;Approve&nbsp;&nbsp;</button>';
				$html .= '</div>';
				$html .= '<div class="col-lg-6 text-left">';
				$html .= '<button class="btn default small" value="true" id="AnsReject" type="submit" name="AnsReject">';
				$html .= '<i class="fa fa-times"></i>&nbsp;&nbsp;Reject&nbsp;&nbsp;</button>';
				$html .= '</div>';
			}
			$html .= '</div>';
			$html .= '</form>';
			$html .= '</div>';
			$html .= '</div>';
			$html .= '</div>';
		}  
		echo $html; 
	}
	//end of the script
}