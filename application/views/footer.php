			</div>
		</div>
	</div>
</div>

<div id="footer">
	<div class="row">
		<div class="col-md-12">
			<p class="text-center">Copyrighted &copy; 2015. All rights reserved.</p>
		</div>
	</div>
</div>

<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/bootstrap-switch.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/bootstrap-datepicker.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/scripts/jquery.dataTables.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/dataTables.bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/scripts/jquery.autocomplete.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/scripts/custom.js')?>"></script>
</body>
</html>