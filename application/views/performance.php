<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a class="active" href="<?=base_url('admin/members')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Current Members <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/add-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Member <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
    <div class="panel-style space">
        <h3 class="heading-title"><i class="fa fa-users"></i> Group Performance</h3>
        <?php if ( $success ) { ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$success?>
            </div><br>
        <?php } else if ( $error ) { ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$error?>
            </div><br>
        <?php } ?>
		<table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th class="text-center">Member #</th>
                    <th>Member Name</th>
                    <th class="text-center">Status</th>
                    <th class="text-center">Performance Sheet</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach($gpmember as $gmember) { ?>			
                    <tr>
                        <td class="text-center"><?php echo $gmember['member']->MID; ?></td>
                        <td><?php echo $gmember['member']->MName; ?></td>
                        <td class="text-center"><?php 
							$statuses = array();
							for ( $i=0; $i<count($gmember['answer']); $i++ ) {
								$statuses[$i] = $gmember['answer'][$i]->AnswerStatus;
							}
							if ( count($statuses) == 0 ) {
								echo 'No Response';
							} elseif (count(array_unique($statuses)) === 1 && end($statuses) === 'Approve') {
								echo 'Approved';
							} elseif (count(array_unique($statuses)) === 1 && end($statuses) === 'Pending') {
								echo 'Pending';
							} else {
								echo count(array_keys($statuses, 'Approve')) . ' Approved';
							} ?>
                        </td>
                        <td class="text-center">
							<a href="#" data-toggle="modal" class="performance_model" data-value="<?php echo $gmember['member']->MID; ?>">View Sheet</a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>


<div class="modal fade" id="PerformanceModal" tabindex="-1" role="dialog" aria-labelledby="performanceModalLabel" aria-hidden="true"></div>