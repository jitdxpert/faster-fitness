<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a class="active" href="<?=base_url('admin/earn-point')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Earn Point <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-gift"></i> Add Earn Point</h3>
		<?php if ( $success ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$success?>
					</div>
				</div><br>
			<?php } else if ( $error ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$error?>
					</div>
				</div><br>
			<?php } ?>
		<div class="row general-fc">
			<?php $attributes = array('name' => 'AddEarnPoint', 'id' => ''); ?>
			<?=form_open(base_url('admin/add-earn-point') . '/', $attributes)?>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Point Topic</label>
						<?php 
						$field = array(
							'name'        	=> 'PTopic',
							'id'          	=> 'PTopic',
							'placeholder' 	=> "Point Topic",
						); ?>
						<?=form_input($field); ?>
						<?=form_error('PTopic', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Points Earn</label>
						<?php 
						$field = array(
							'name'        	=> 'PEarn',
							'id'          	=> 'PEarn',
							'placeholder' 	=> "Points Earn",
						); ?>
						<?=form_input($field); ?>
						<?=form_error('PEarn', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-6"><br />
					<?php
					$field = array(
						'name' 		=> 'PSave',
						'id' 		=> 'PSave',
						'value' 	=> 'true',
						'type' 		=> 'submit',
						'content' 	=> '<i class="fa fa-save"></i>&nbsp;&nbsp;Save&nbsp;&nbsp;',
						'class'		=> 'btn green small',
					); ?>					
					<?=form_button($field) ?>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>