<div class="profile-categories">
	<div class="search-block clearfix">
		<?php $attributes = array('name' => 'SearchBox', 'id' => 'SearchBox', 'autocomplete' => 'off'); ?>
        <?=form_open(base_url('admin/members') . '/', $attributes)?>
			<div class="form-group clearfix">
				<i class="fa fa-search"></i>
				<?php 
				$field = array(
					'name'        	=> 'member',
					'id'          	=> 'MName',
					'placeholder' 	=> "Member Name",
				); ?>
				<?=form_input($field); ?>
			</div>
			<div class="form-group clearfix">
				<i class="fa fa-calendar"></i>
				<?php 
				$field = array(
					'name'        		=> 'date',
					'id'          		=> 'date',
					'placeholder' 		=> "06/12/2015",
					'data-provide' 		=> 'datepicker',
					'data-date-format' 	=> 'yyyy-mm-dd',
					'readonly'			=> 'true',
					'value'				=> set_value('date'),
				); ?>
				<?=form_input($field); ?>
			</div>
			<div class="form-group clearfix">
				<?php
				$field = array(
					'name' 		=> 'search',
					'id' 		=> 'search',
					'value' 	=> 'true',
					'type' 		=> 'submit',
					'content' 	=> 'Search',
					'class'		=> 'btn green small btn-block',
				); ?>					
				<?=form_button($field) ?>
			</div>
		<?=form_close()?>
	</div>
</div>