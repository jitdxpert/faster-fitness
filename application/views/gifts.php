<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
        <h3 class="grey-title">CUSTOM MENU</h3>
        <ul>
            <li>
                <a class="active" href="<?=base_url('admin/gifts')?>/">
                    <i class="fa fa-list-ul"></i>&nbsp;&nbsp;All Gifts <span></span>
                </a>
            </li>
            <li>
                <a href="<?=base_url('admin/add-gift')?>/">
                    <i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Gift <span></span>
                </a>
            </li>
        </ul>	
    </div>
</div>

<div class="col-md-9">
    <div class="panel-style space">
        <h3 class="heading-title"><i class="fa fa-gift"></i> Gifts</h3>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Gift #</th>
                    <th>Gift Name</th>
                    <th>Gift Points</th>
                    <th>Gift Notes</th>
                    <th>Gift Date</th>
                    <th>Gift Image</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ( $gifts as $gift ) { ?>
                    <tr>
                        <td><?=$gift->GftID?></td>
                        <td><?=$gift->GftName?></td>
                        <td><?=$gift->GftPoints?></td>
                        <td><?=$gift->GftNotes?></td>
                        <td><?=date('d-m-Y', strtotime($gift->GftDate))?></td>
                        <td><img alt="" src="<?=base_url('assets/gifts/50x50_'.$gift->GftPicName)?>"></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>