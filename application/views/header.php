<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />

<title><?=$title?></title>

<link rel="stylesheet" href="http://brick.a.ssl.fastly.net/Raleway:100,200,300,400,500,600,700,800,900,200i,300i,400i,500i" />

<link href="<?=base_url('assets/bootstrap/css/bootstrap.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/bootstrap/css/bootstrap-switch.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/bootstrap/css/bootstrap-datepicker.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/bootstrap/css/dataTables.bootstrap.min.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/fonts/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/styles/jquery.autocomplete.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/style.css')?>" rel="stylesheet" />

<script type="text/javascript" src="<?=base_url('assets/scripts/jquery-1.8.3.js')?>"></script>
<input type="hidden" name="base_url" id="base_url" value="<?php echo base_url(); ?>" />
</head>

<body>
<nav class="navbar navbar-default" role="navigation">
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="<?=base_url()?>">
			<img src="<?=base_url('assets/images/logo.png')?>" alt="" />
			<span id="sidebar-exp"></span>
		</a>
	</div>
	<div class="collapse navbar-collapse navbar-ex1-collapse">
		<ul class="nav navbar-nav navbar-right header-drop-right">
			<li class="dropdown user-dropdown-one">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					Admin <img src="<?=base_url('assets/images/user-img.png')?>" alt="" class="img-user" />
					<b class="caret"></b>
				</a>
				<ul class="dropdown-menu user-dropdown-two">
					<li><a href="<?=base_url()?>">Settings</a></li>
					<li><a href="<?=base_url()?>">Profile</a></li>
					<li class="log-out"><a href="<?=base_url('admin/logout')?>/">Logout</a></li>
				</ul>
			</li>
		</ul>
		
		<ul class="header-notifications pull-right">
			<li class="dropdown group-dropdown-one">
				<a href="#" class="dropdown-toggle <?=($this->uri->segment(2)=='members')?'active':''?>" data-toggle="dropdown">Members <b class="caret"></b></a>
				<ul class="dropdown-menu group-dropdown-two">
					<li><a class="<?=($this->uri->segment(2)=='members')?'active':''?>" href="<?=base_url('admin/members')?>/">Members</a></li>
					<li><a class="<?=($this->uri->segment(2)=='add-member')?'active':''?>" href="<?=base_url('admin/add-member')?>/">Existing Member</a></li>
					<li><a class="<?=($this->uri->segment(2)=='new-member')?'active':''?>" href="<?=base_url('admin/new-member')?>/">New Member</a></li>
				</ul>
			</li>
			<li><a class="<?=($this->uri->segment(2)=='exercises')?'active':''?>" href="<?=base_url('admin/exercises')?>/">Exercises</a></li>
			<li class="dropdown group-dropdown-one">
				<a href="#" class="dropdown-toggle <?=($this->uri->segment(2)=='groups')?'active':''?>" data-toggle="dropdown">Groups <b class="caret"></b></a>
				<ul class="dropdown-menu group-dropdown-two">
					<li><a class="<?=($this->uri->segment(2)=='groups')?'active':''?>" href="<?=base_url('admin/groups')?>/">Groups</a></li>
					<li><a class="<?=($this->uri->segment(2)=='add-group')?'active':''?>" href="<?=base_url('admin/add-group')?>/">Add Group</a></li>
				</ul>
			</li>
			<li><a class="<?=($this->uri->segment(2)=='leaderboard')?'active':''?>" href="<?=base_url('admin/leaderboard')?>/">Leaderboard</a></li>
			<li><a class="<?=($this->uri->segment(2)=='gifts')?'active':''?>" href="<?=base_url('admin/gifts')?>/">Gifts</a></li>
		</ul>
	</div>
</nav>

<div id="wrapper">
	<div id="body-overlay"></div>
	<div id="sidebar">
		<div class="search-block clearfix">
			<form action="" method="post" role="form">
				<span class="fa fa-search"></span>
				<input type="text" placeholder="Search member name" />
			</form>
		</div>
		<ul class="drop-area">
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='members')?'active':''?>" href="<?=base_url('admin/members')?>/">Members</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='exercises')?'active':''?>" href="<?=base_url('admin/exercises')?>/">Exercises</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='groups')?'active':''?>" href="<?=base_url('admin/groups')?>/">Groups</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='add-group')?'active':''?>" href="<?=base_url('admin/add-group')?>/">Add Group</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='leaderboard')?'active':''?>" href="<?=base_url('admin/leaderboard')?>/">Leaderboard</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='earn-point')?'active':''?>" href="<?=base_url('admin/earn-point')?>/">Earn Points</a>
			</li>
			<li class="no-back">
				<a class="<?=($this->uri->segment(2)=='gifts')?'active':''?>" href="<?=base_url('admin/gifts')?>/">Gifts</a>
			</li>
		</ul>
	</div>
    
	<div id="wrap">
		<div class="container">
			<div class="row m-top-55">