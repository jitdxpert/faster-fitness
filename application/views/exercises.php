<div class="col-md-3">
	<div class="panel-style space custom-menu no-pad-r">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a class="active" href="<?=base_url('admin/exercises')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Exercises <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/add-exercise')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Exercise <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>


<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-hospital-o"></i> Exercises</h3>
		<?php if ( $success ) { ?>
			<div class="alert alert-success" role="alert">
				<i class="fa fa-info-circle"></i> <?=$success?>
			</div><br>
		<?php } elseif ( $error ) { ?>
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-info-circle"></i> <?=$error?>
			</div><br>
		<?php } ?>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Exercise Name</th>
                    <th>Exercise Unit</th>
                    <th>Exercise Unit Type</th>
					<th>Exercise Question</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($exercises as $exercise){ ?>
                    <tr>
                        <td><?php echo $exercise->EName; ?></td>
                        <td><?php echo $exercise->EUnit; ?></td>
                        <td><?php echo 	$exercise->EUnitType; ?></td>
						<td><?php echo 	$exercise->EQuestion; ?></td>
                        <input type="hidden" name="EID" value="<?php echo $exercise->EID; ?>" />
                        <td class="text-center">
                            <a href="#" data-toggle="modal" class="btn btn-xs green ex_model" data-value="<?php echo $exercise->EID; ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                            <a href="<?=base_url('admin/delete_exercise/'.$exercise->EID);?>/" class="btn btn-xs btn-danger" onclick="return confirm('Are you sure you want to delete this data?');">
                                <i class="fa fa-trash-o"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>
<div class="modal fade" id="ExerciseModal" tabindex="-1" role="dialog" aria-labelledby="exerciseModalLabel" aria-hidden="true"></div>