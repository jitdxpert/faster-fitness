<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
        <h3 class="grey-title">CUSTOM MENU</h3>
        <ul>
            <li>
                <a href="<?=base_url('admin/groups')?>/">
                    <i class="fa fa-list-ul"></i>&nbsp;&nbsp;Current Groups <span></span>
                </a>
            </li>
            <li>
                <a class="active" href="<?=base_url('admin/add-group')?>/">
                    <i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Group <span></span>
                </a>
            </li>
        </ul>	
    </div>
</div>

<div class="col-md-9">
    <div class="panel-style space">
        <h3 class="heading-title"><i class="fa fa-user"></i> Add Group</h3>
        <div class="row general-fc">
        	<?php if ( $success ) { ?>
                <div class="col-lg-12">
                    <div class="alert alert-success" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <i class="fa fa-info-circle"></i> <?=$success?>
                    </div>
                </div><br>
            <?php } else if ( $error ) { ?>
                <div class="col-lg-12">
                    <div class="alert alert-danger" role="alert">
                        <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <i class="fa fa-info-circle"></i> <?=$error?>
                    </div>
                </div><br>
            <?php } ?>
            
            <?php $attributes = array('name' => 'AddGroup', 'id' => 'AddGroup'); ?>
            <?=form_open_multipart(base_url('admin/add-group') . '/', $attributes)?>
                <div class="col-md-6">
                    <div class="input-f-wrapper">
                        <label>Group Name: </label>
                        <?php 
						$field = array(
							'name'  => 'GrName',
							'id'    => 'GrName',
							'placeholder' => "Group Name",
						);
						echo form_input($field); ?>
						<?=form_error('GrName', '<span class="error">', '</span>')?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="input-t-wrapper">
                        <label>Group Description: </label>
                        <?php
                         $field = array(
                          'name'        => 'GrText',
                          'id'          => 'GrText',
                          'rows'        => '100',
                          'cols'        => '100',
                          'style'       => 'width: 394px; margin-left: 0px; margin-right: -2.25px;'
                        );
                        echo form_textarea($field); ?>
                        <?=form_error('GrText', '<span class="error">', '</span>')?>
                    </div>
                </div>
				<div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="input-f-wrapper">
                        <label>Group Image: </label>
                        <?php 
							$field = array(
								'name'  => "GPicName",
								'id'    => "GPicName",
								'class'	=> 'form-control',
							); ?>
							<?=form_upload($field)?>
							<font size="1">Max Size 1MB with GIF, JPG, PNG, BMP</font>
                    </div>
                </div>	
                <div class="clearfix"></div>
                <div class="col-md-12"><br />
                    <?php
                      $field = array(
                          'name'    => 'GSubmit',
                          'id' 	    => 'GSubmit',
                          'value'   => 'true',
                          'type'    => 'submit',
                          'content' => '<i class="fa fa-save"></i>&nbsp;&nbsp;Save Group&nbsp;&nbsp;',
                          'class'   => 'btn green small',
                     ); ?>				
                 <?= form_button($field) ?>
                </div>
            <?=form_close()?>
        </div>
    </div>
</div>