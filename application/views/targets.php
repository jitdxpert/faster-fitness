<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<?php foreach($gpdata as $gdata){ ?>
        <div class="profile-info">
            <img alt="" src="<?=base_url('assets/groups/250x150_'.$gdata->GPicName)?>">
            <div class="info">
                <h3><?=$gdata->GName; ?></h3>
            </div>
		</div>
	<?php } ?>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-check-square-o"></i> Targets</h3>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Exercise</th>
                    <th class="text-center">Monday</th>
                    <th class="text-center">Tuesday</th>
                    <th class="text-center">Wednesday</th>
                    <th class="text-center">Thursday</th>
                    <th class="text-center">Friday</th>
                    <th class="text-center">Saturday</th>
                    <th class="text-center">Sunday</th>
                </tr>
            </thead>
            <tbody>
                <?php for ( $i = 0; $i < count($exdata); $i++ ) { ?>
                    <tr class="ExerciseRow">
                        <td>
                            <?=$exdata[$i]->EName?>
                            <input type="hidden" name="EID" value="<?=$exdata[$i]->EID?>" />
                            <input type="hidden" name="GID" value="<?=$gdata->GID?>" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TMonday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TTuesday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TWednesday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TThursday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TFriday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TSaturday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                        <td class="text-center">
                            <input type="hidden" name="days" value="TSunday" />
                            <input type="checkbox" data-size="mini" checked="false" class="BootSwitch" />
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>