<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a class="active" href="<?=base_url('admin/members')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Current Members <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/add-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Member <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
    <div class="panel-style space">
        <h3 class="heading-title"><i class="fa fa-users"></i> Group Performance</h3>
		<table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Member #</th>
                    <th>Member Name</th>
                    <th>Status</th>
                    <th><class="text-center">Performance Sheet</th>
                </tr>
            </thead>
            <tbody>
				<?php foreach($gpmember as $gmember) { ?>			
                    <tr>
                        <td><?php echo $gmember->MID; ?></td>
                        <td><?php echo $gmember->MName; ?></td>
                        <td>Pending</td>
                        <td><class="text-center">
                            <a href="#" data-toggle="modal" data-target="#myModal" type="button">View Sheet</a></td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content">
        	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Member Performance</h4>
            </div>
            <div class="modal-body">
            	List of Exercises done in last XX days:<br /><br />
            	<table class="table table-bordered table-striped">
                	<tbody>
						<tr>
                        	<th>Exercise Name</th>
                            <th>Exercise Unit</th>
                            <th>Exercise Unit Type</th>
                        </tr>
                        <tr>
                        	<td>Exercise #001</td>
                            <td>Push Up Test</td>
                            <td>04 Min</td>
                        </tr>
                        <tr>
                        	<td>Exercise #002</td>
                            <td>Push Up Test</td>
                            <td>04 Min</td>
                        </tr>
                        <tr>
                        	<td>Exercise #003</td>
                            <td>Push Up Test</td>
                            <td>04 Min</td>
                        </tr>
                        <tr>
                        	<td>Exercise #004</td>
                            <td>Push Up Test</td>
                            <td>04 Min</td>
                        </tr>
                        <tr>
                        	<td>&nbsp;</td>
                            <td>
                                <?php
                                $field = array(
                                    'name' 		=> 'MUpdate',
                                    'id' 		=> 'MUpdate',
                                    'value' 	=> 'true',
                                    'type' 		=> 'submit',
                                    'content' 	=> '<i class="fa fa-save"></i>&nbsp;&nbsp;Approve&nbsp;&nbsp;',
                                    'class'		=> 'btn green small',
                                ); ?>					
                                <?=form_button($field) ?>
                            </td>
                            <td>
                                <?php
                                $field = array(
                                    'name' 		=> 'MUpdate',
                                    'id' 		=> 'MUpdate',
                                    'value' 	=> 'true',
                                    'type' 		=> 'submit',
                                    'content' 	=> '<i class="fa fa-save"></i>&nbsp;&nbsp;Reject&nbsp;&nbsp;',
                                    'class'		=> 'btn green small',
                                ); ?>					
                                <?=form_button($field) ?>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>