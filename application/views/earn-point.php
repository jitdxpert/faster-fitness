<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
		    <li>
				<a class="active" href="<?=base_url('admin/earn-point')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Earn Point <span></span>
				</a>
			</li>
		</ul>
	</div>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-hospital-o"></i> Events</h3>
		<?php if ( $success ) { ?>
			<div class="alert alert-success" role="alert">
				<i class="fa fa-info-circle"></i> <?=$success?>
			</div><br>
		<?php } elseif ( $error ) { ?>
			<div class="alert alert-danger" role="alert">
				<i class="fa fa-info-circle"></i> <?=$error?>
			</div><br>
		<?php } ?>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Event Name</th>
                    <th>Earn Points</th>
                    <th class="text-center">Actions</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($earnpoint as $ep){ ?>
                    <tr>
                        <td><?php echo $ep->EarnTopic; ?></td>
                        <td><?php echo $ep->EarnPoints; ?></td>
                        <input type="hidden" name="EarnID" value="<?php echo $ep->EarnID; ?>" />
                        <td class="text-center">
                            <a href="#" data-toggle="modal" class="btn btn-xs green point_model" data-value="<?php echo $ep->EarnID; ?>">
                                <i class="fa fa-pencil"></i>
                            </a>
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>  
<div class="modal fade" id="pointModal" tabindex="-1" role="dialog" aria-labelledby="pointModalLabel" aria-hidden="true"></div>