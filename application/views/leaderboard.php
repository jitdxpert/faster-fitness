<div class="col-md-12">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-gift"></i> Leaderboard</h3>
		<table class="table table-bordered simple m-bot-0 dataTable">
			<thead>
				<tr>
					<th>Position</th>
					<th>Member Name</th>                                   
					<th>Earn Points</th>
					<th>Eligible For</th>
				</tr>
			</thead>
			<tbody>
			<?php $i = 1; foreach ( $leaderboard as $board ) { ?>
				<tr>
					<td><?=$i?></td>
					<td><?=$board->MName?></td>
					<td><?=$board->MEarnPoint?></td>
					<td><?=$board->Giftname?></td>
				</tr>
				</tr>
				<?php $i++; } ?>
			</tbody>
		</table>
	</div>
</div>