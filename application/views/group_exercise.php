<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
</div>
<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-hospital-o"></i> Edit Exercise</h3>
		<div class="row general-fc">
			<?php if ( $success ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$success?>
					</div>
				</div><br>
			<?php } else if ( $error ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$error?>
					</div>
				</div><br>
			<?php } ?>
		</div>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th>Exercise Name</th>
                    <th>Default Exercise Unit</th>
                    <th>Group Exercise Unit</th>
                    <th>Exercise Unit Type</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ( $group_exercise as $exercise ) { ?>
                    <tr>
                        <td><?=$exercise->EName?></td>
                        <td><?=$exercise->EUnit?></td>
                        <td><?=$exercise->TEUnit?></td>
                        <td><?=$exercise->EUnitType?></td>
                        <td class="text-center">
                            <a href="#" data-toggle="modal" class="btn btn-xs green gr_ex_model" data-exercise="<?=$exercise->EID?>" data-group="<?=$exercise->TGID?>">
                                <i class="fa fa-pencil"></i>
                            </a> 
                        </td>
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div> 
<div class="modal fade" id="GroupExerciseModal" tabindex="-1" role="dialog" aria-labelledby="groupexerciseModalLabel" aria-hidden="true"></div>