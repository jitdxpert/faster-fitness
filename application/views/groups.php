<?php if(!empty($groups)){ ?>
	<?php foreach ( $groups as $group ) { ?>
		<div class="col-md-3">
			<div class="profile-info">
				<img alt="" src="<?=base_url('assets/groups/250x150_'.$group->GPicName)?>">
				<div class="info">
					<h3><?php echo $group->GName;?></h3>
				</div>
				<div class="info">
					<a href="<?=base_url('admin/targets/'.$group->GID.'')?>/" class="btn green small btn-block">Set Target</a>
					<a href="<?=base_url('admin/performance/'.$group->GID.'')?>/" class="btn green small btn-block">Approve Performance</a>
					<a href="<?=base_url('admin/group-exercise/'.$group->GID.'')?>/" class="btn green small btn-block">Exercises</a>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } else { ?>
	<div class="alert alert-danger" role="alert">
		<i class="fa fa-info-circle"></i> There is no group to display.
	</div>
<?php } ?>