<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a class="active" href="<?=base_url('admin/members')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Current Members <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/add-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Existing Member <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/new-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add New Member <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-users"></i> Members</h3>
        <?php if ( $success ) { ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$success?>
            </div><br>
        <?php } else if ( $error ) { ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$error?>
            </div><br>
        <?php } else if ( $this->input->post('search') ) { ?>
			<div class="alert alert-info" role="alert">
                Search results for : 
				<?php if ( $this->input->post('member') || $this->input->post('date') ) { ?>
					<?=($this->input->post('member')?$this->input->post('member').' ':'')?>
					<?=($this->input->post('date')?$this->input->post('date'):'')?>
				<?php } else { ?>
					all members
				<?php } ?>
            </div><br>
		<?php } ?>
        <table class="table table-bordered simple m-bot-0 dataTable">
            <thead>
                <tr>
                    <th width="5%">#</th>
                    <th width="15%">Name</th>
                    <th width="20%">Email</th>
                    <th width="10%">Mobile</th>
                    <th width="10%">APP Code</th>
                    <th width="12%">Join Date</th>
                    <th width="10%" class="text-center">Weight</th>
					<th width="10%" class="text-center">Payment Status</th>
                    <th width="8%"></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ( $members as $member ) { ?>
                    <tr>
                        <td><?=$member->MID?></td>
                        <td><?=$member->MName?></td>
                        <td><?=$member->MEmail?></td>
                        <td><?=$member->MNumber?></td>
                        <td><a><?=$member->MAPPCode?></a></td>
                        <td><?=date('d-m-Y', strtotime($member->MCreated))?></td>
                        <input type="hidden" name="EarnID" value="<?php echo $member->MID; ?>" />
                        <td class="text-center">
                            <a href="#" data-toggle="modal" class="weight_model" data-value="<?php echo $member->MID; ?>">
                                <?=$member->MWeight?> kgs
                            </a>
                        </td>
						<td class="text-center">
						<?php if($member->MStatus == "Unpaid"){ ?>
						    <a href="#" data-toggle="modal" class="payment_model" data-value="<?php echo $member->MID; ?>">
                            	<i class="fa fa-times text-danger"></i>
                                <span style="display:block;font-size:10px;">Click to edit</span>
                            </a>
						<?php } else { ?>
							<i class="fa fa-check text-success"></i>
						<?php } ?>
                            
                        </td>
						
                        <td class="text-center">
                            <a href="#" data-toggle="modal" class="btn green btn-xs member_model" data-value="<?php echo $member->MID; ?>"><i class="fa fa-pencil"></i></a>
                        </td>		
                    </tr>
                <?php } ?>
            </tbody>
        </table>
	</div>
</div>
<div class="modal fade" id="memberModal" tabindex="-1" role="dialog" aria-labelledby="memberModalLabel" aria-hidden="true"></div>
<div class="modal fade" id="weightModal" tabindex="-1" role="dialog" aria-labelledby="weightModalLabel" aria-hidden="true"></div>
<div class="modal fade" id="paymentModal" tabindex="-1" role="dialog" aria-labelledby="paymentModalLabel" aria-hidden="true"></div>