<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a href="<?=base_url('admin/members')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Current Members <span></span>
				</a>
			</li>
			<li>
				<a href="<?=base_url('admin/add-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Existing Member <span></span>
				</a>
			</li>
			<li>
				<a class="active" href="<?=base_url('admin/new-member')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add New Member <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>
				
<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-user"></i> New Member <small class="pull-right"><span class="require">*</span> sign fields are mandatory.</small></h3>
		<div class="row general-fc">
				<div class="col-md-12">
					<div class="input-f-wrapper">
						<label>APP Code: </label>
						<div class="clearfix"></div>
						<input type="text" placeholder="APP Code" style="float:left;width:60%;" />
						<button class="btn green small" id="get_app_data"  style="margin-left:-5px;padding: 10.5px 12px;border-radius: 0 2px 2px 0;border:none;float:left;">
							<i class="fa fa-search"></i>
						</button> 
						<span style="font-size:28px;margin-left:15px">
							<i class="fa fa-check text-success hide"></i> 
							<i class="fa fa-times text-danger hide"></i>
						</span>
					</div>
				</div>
				<?php if ( $success ) { ?>
					<div class="col-lg-12">
						<div class="alert alert-success" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<i class="fa fa-info-circle"></i> <?=$success?>
						</div>
					</div><br>
				<?php } else if ( $error ) { ?>
					<div class="col-lg-12">
						<div class="alert alert-danger" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							<i class="fa fa-info-circle"></i> <?=$error?>
						</div>
					</div><br>
				<?php } ?>
				<?php $attributes = array('name' => 'MemberForm', 'id' => 'MemberForm2'); ?>
				<?=form_open(base_url('admin/new-member') . '/', $attributes)?>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="RefUserID">Referred By<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'RefUserID',
							'id'          	=> 'RefUserID',
							'placeholder' 	=> "Referred By",
							'value'			=> set_value('RefUserID'),
							'readonly'		=> 'true',
						); ?>
						<?=form_input($field); ?>
						<?=form_error('RefUserID', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MAPPCode">Free Trial Days: </label>
						<?php 
						$field = array(
							'name'        	=> 'MTrialDays',
							'id'          	=> 'MTrialDays',
							'placeholder' 	=> "Free Trial Days",
							'value'			=> set_value('MTrialDays'),
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MTrialDays', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MName">Member Name<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'MName',
							'id'          	=> 'MName',
							'placeholder' 	=> "Member Name",
							'value'			=> set_value('MName'),
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MName', '<span class="error">', '</span>')?>
						<?=form_hidden('MType', 'Existing')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MNumber">Contact Number<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'MNumber',
							'id'          	=> 'MNumber',
							'placeholder' 	=> "Contact Number",
							'value'			=> set_value('MNumber'),							
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MNumber', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MEmail">Email ID<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'MEmail',
							'id'          	=> 'MEmail',
							'placeholder' 	=> "Email ID",
							'value'			=> set_value('MEmail'),	
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MEmail', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MAddress">Member Address<span class="require">*</span>: </label>
						<div class="wrap-location-input">
							<?php 
							$field = array(
								'name'        	=> 'MAddress',
								'id'          	=> 'MAddress',
								'placeholder' 	=> "Member Address",
								'value'			=> set_value('MAddress'),	
							); ?>
							<?=form_input($field); ?>
							<a></a>
							<?=form_error('MAddress', '<span class="error">', '</span>')?>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MAge">Age<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'MAge',
							'id'          	=> 'MAge',
							'placeholder' 	=> "Age",
							'value'			=> set_value('MAge'),
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MAge', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-r-wrapper">
						<label for="MGender">Gender<span class="require">*</span>: </label>
						<div class="row">
							<div class="col-md-6">
								<label for="MMale">
									<?php
									$data = array(
										'name'      => 'MGender',
										'id'        => 'MMale',
										'value'     => 'Male',
										'checked'	=> (set_value('MGender')=='Male' ? 'checked' : 'checked'),
									); ?>
									<?=form_radio($data); ?> Male
								</label>
							</div>
							<div class="col-md-6">
								<label for="MFemale">
									<?php
									$data = array(
										'name'      => 'MGender',
										'id'        => 'MFemale',
										'value'     => 'Female',
										'checked'	=> (set_value('MGender')=='Female' ? 'checked' : ''),
									); ?>
									<?=form_radio($data); ?> Female
								</label>
							</div>
						</div>
						<?=form_error('MGender', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MWeight">Weight<span class="require">*</span>: </label>
						<?php 
						$field = array(
							'name'        	=> 'MWeight',
							'id'          	=> 'MWeight',
							'placeholder' 	=> "Your Weight",
							'value'			=> set_value('MWeight'),	
						); ?>
						<?=form_input($field); ?>
						<?=form_error('MWeight', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label for="MAPPCode">Choose member group: </label>
						<select class="form-control" name="MGpid">
							<option>Please select the member group</option>
							<?php foreach ($groups as $group) { 
								echo '<option value="'.$group->GID.'">'.$group->GName.'</option>';
							} ?>
						</select>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-r-wrapper">
						<label for="MStatus">Payment Status<span class="require">*</span>: </label>
						<div class="row">
							<div class="col-md-6">
								<label for="MPaid">
									<?php
									$data = array(
										'name'          => 'MStatus',
										'id'            => 'MPaid',
										'value'         => 'paid',
										'checked'		=> (set_value('MStatus')=='paid' ? 'checked' : 'checked'),
									); ?>
									<?=form_radio($data); ?> Paid
								</label>
							</div>
							<div class="col-md-6">
								<label for="MUnpaid">
									<?php
									$data = array(
										'name'          => 'MStatus',
										'id'            => 'MUnpaid',
										'value'         => 'unpaid',
										'checked'		=> (set_value('MStatus')=='unpaid' ? 'checked' : ''),
									); ?>
									<?=form_radio($data); ?> Unpaid
								</label>
							</div>
						</div>
						<?=form_error('MStatus', '<span class="error">', '</span>')?>
					</div>
				</div>
				
				<input type="hidden" name="RefferUserID" id="RefferUserID" value="" />
				<input type="hidden" name="newAddID" id="newAddID" value="" />	
				<input type="hidden" name="RequestID" id="RequestID" value="" />
				<input type="hidden" name="RefStatus" id="RefStatus" value="" />
									
				<div class="clearfix"></div>
				<div class="col-md-12">
					<?php
					$field = array(
						'name' 		=> 'MSubmit',
						'id' 		=> 'MSubmit',
						'value' 	=> 'true',
						'type' 		=> 'submit',
						'content' 	=> '<i class="fa fa-save"></i>&nbsp;&nbsp;SAVE&nbsp;&nbsp;',
						'class'		=> 'btn green small',
					); ?>					
					<?=form_button($field) ?>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>