<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a href="<?=base_url('admin/gifts')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;All Gifts <span></span>
				</a>
			</li>
			<li>
				<a class="active" href="<?=base_url('admin/add-gift')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Gift <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-gift"></i> Add Gift</h3>
		<div class="row general-fc">
			<?php if ( $success ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$success?>
					</div>
				</div><br>
			<?php } else if ( $error ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$error?>
					</div>
				</div><br>
			<?php } ?>
			<?php $attributes = array('name' => 'AddGift', 'id' => 'AddGift'); ?>
			<?=form_open_multipart(base_url('admin/add-gift') . '/', $attributes)?>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Gift Name</label>
						<?php 
						$field = array(
							'name'        	=> 'GName',
							'id'          	=> 'GName',
							'placeholder' 	=> "Gift Name",
							'value'			=> set_value('GName') ,
						); ?>
						<?=form_input($field); ?>
						<?=form_error('GName', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Gift Notes</label>
						<?php 
						$field = array(
							'name'        	=> 'GNotes',
							'id'          	=> 'GNotes',
							'placeholder' 	=> "Gift Notes",
							'value'			=> set_value('GNotes') ,
						); ?>
						<?=form_input($field); ?>
						<?=form_error('GNotes', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Gift Points</label>
						<?php 
						$field = array(
							'name'        	=> 'GPoints',
							'id'          	=> 'GPoints',
							'placeholder' 	=> "Gift Points",
							'value'			=> set_value('GPoints') ,
						); ?>
						<?=form_input($field); ?>
						<?=form_error('GPoints', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Gift Date</label>
						<?php 
						$field = array(
							'name'        	=> 'GDate',
							'id'          	=> 'GDate',
							'placeholder' 	=> "Gift Date",
							'data-provide' 		=> 'datepicker',
							'data-date-format' 	=> 'yyyy-mm-dd',
							'value'				=> set_value('GDate'),
							'readonly'			=> 'true',
						); ?>
						<?=form_input($field); ?>
						<?=form_error('GDate', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="input-f-wrapper">
                        <label>Gift Image: </label>
                        <?php 
						$field = array(
							'name'  => "GftPicName",
							'id'    => "GftPicName",
							'class'	=> 'form-control',
						); ?>
						<?=form_upload($field)?>
						<font size="1">Max Size 1MB with GIF, JPG, PNG, BMP</font>
                    </div>
                </div>
				<div class="clearfix"></div>
				<div class="col-md-6"><br />
					<?php
					$field = array(
						'name' 		=> 'GSave',
						'id' 		=> 'GSave',
						'value' 	=> 'true',
						'type' 		=> 'submit',
						'content' 	=> '<i class="fa fa-save"></i>&nbsp;&nbsp;Save Gift&nbsp;&nbsp;',
						'class'		=> 'btn green small',
					); ?>					
					<?=form_button($field) ?>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>