<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />

<title><?=$title?></title>

<link rel="stylesheet" href="http://brick.a.ssl.fastly.net/Raleway:100,200,300,400,500,600,700,800,900,200i,300i,400i,500i" />

<link href="<?=base_url('assets/bootstrap/css/bootstrap.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/fonts/font-awesome/css/font-awesome.min.css')?>" rel="stylesheet" />
<link href="<?=base_url('assets/style.css')?>" rel="stylesheet" />

<script src="<?=base_url('assets/scripts/jquery.min.js')?>"></script>
</head>

<body class="login">
<div id="login">
	<div class="logo">
		<a href="<?=base_url()?>"><img src="<?=base_url('assets/images/logo-login.png')?>" alt="" /></a>
	</div>
	<?=form_open(base_url('admin') . '/')?>
    	<?php if ( $success ) { ?>
            <div class="alert alert-success" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$success?>
            </div><br>
        <?php } else if ( $error ) { ?>
            <div class="alert alert-danger" role="alert">
                <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <i class="fa fa-info-circle"></i> <?=$error?>
            </div><br>
        <?php } ?>
		<div class="wrapper-login">
			<?php 
			$field = array(
				'name'        => 'Email',
				'id'          => 'Email',
				'placeholder' => 'Enter Your Email',
				'class'		  => 'user',
			); ?>
            <?=form_error('Email', '<span class="error">', '</span>')?>
			<?=form_input($field)?>
			<?php
			$field = array(
				'name'        => 'Password',
				'id'          => 'Password',
				'placeholder' => 'Enter Your Password',
				'class'		  => 'password',
			); ?>
            <?=form_error('Password', '<span class="error">', '</span>')?>
			<?=form_password($field)?>			
		</div>
        <?php
		$field = array(
			'name' 		=> 'LoginSubmit',
			'id' 		=> 'LoginSubmit',
			'value' 	=> 'true',
			'type' 		=> 'submit',
			'content' 	=> '<i class="fa fa-sign-in"></i> Log in',
			'class'		=> 'login-button',
		); ?>					
		<?= form_button($field) ?>
	<?=form_close()?>
	<span><a href="#">Forgot your Password</a></span>
</div>
	
<script type="text/javascript" src="<?=base_url('assets/bootstrap/js/bootstrap.min.js')?>"></script>
<script type="text/javascript" src="<?=base_url('assets/scripts/custom.js')?>"></script>
</body>
</html>