<div class="col-md-3">
	<?php $this->load->view('searchbox'); ?>
	<div class="panel-style space custom-menu no-pad-r m-bot-30 m-top-30">
		<h3 class="grey-title">CUSTOM MENU</h3>
		<ul>
			<li>
				<a href="<?=base_url('admin/exercises')?>/">
					<i class="fa fa-list-ul"></i>&nbsp;&nbsp;Exercises <span></span>
				</a>
			</li>
			<li>
				<a class="active" href="<?=base_url('admin/add-exercise')?>/">
					<i class="fa fa-plus-square-o"></i>&nbsp;&nbsp;&nbsp;Add Exercise <span></span>
				</a>
			</li>
		</ul>	
	</div>
</div>

<div class="col-md-9">
	<div class="panel-style space">
		<h3 class="heading-title"><i class="fa fa-hospital-o"></i> Add Exercise</h3>
		<div class="row general-fc">
			<?php if ( $success ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-success" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$success?>
					</div>
				</div><br>
			<?php } else if ( $error ) { ?>
				<div class="col-lg-12">
					<div class="alert alert-danger" role="alert">
						<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						<i class="fa fa-info-circle"></i> <?=$error?>
					</div>
				</div><br>
			<?php } ?>
			<?php $attributes = array('name' => 'ExerciseForm', 'id' => 'ExerciseForm'); ?>
			<?=form_open(base_url('admin/add-exercise') . '/', $attributes)?>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Exercise Name: </label>
						<?php 
						$field = array(
							'name'  => 'EName',
							'id'    => 'EName',
							'placeholder' => "Exercise Name",
						);
						echo form_input($field); ?>
						<?=form_error('EName', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Exercise Unit Type</label>
						<?php 
						$field = array(
							'name'  => 'EUnitType',
							'id'    => 'EUnitType',
							'placeholder' => "Exercise Unit Name",
						);
						echo form_input($field); ?>
						<?=form_error('EUnitType', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Exercise Unit</label>
						<?php 
						$field = array(
							'name'  => 'EUnit',
							'id'    => 'EUnit',
							'placeholder' => "Exercise Unit",
						);
						echo form_input($field); ?>
						<?=form_error('EUnit', '<span class="error">', '</span>')?>
					</div>
				</div>								
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Exercise Notes</label>
						<?php 
						$field = array(
							'name'  => 'ENotes',
							'id'    => 'ENotes',
							'placeholder' => "Exercise Notes",
						);
						echo form_input($field); ?>
						<?=form_error('ENotes', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="col-md-6">
					<div class="input-f-wrapper">
						<label>Exercise Question</label>
						<?php 
						$field = array(
							'name'  => 'EQuestion',
							'id'    => 'EQuestion',
							'placeholder' => "Exercise Question",
						);
						echo form_input($field); ?>
						<?=form_error('EQuestion', '<span class="error">', '</span>')?>
					</div>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12">
					<?php
					$field = array(
						'name'    => 'ESubmit',
						'id' 	    => 'ESubmit',
						'value'   => 'true',
						'type'    => 'submit',
						'content' => '<i class="fa fa-save"></i>&nbsp;&nbsp;Save Exercise&nbsp;&nbsp;',
						'class'   => 'btn green small',
					); ?>					
					<?= form_button($field) ?>
				</div>
			<?=form_close()?>
		</div>
	</div>
</div>