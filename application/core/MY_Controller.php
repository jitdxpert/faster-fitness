<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {
	
	// Start of constructor function
	public function __construct() {
        parent::__construct();
		
		$this->load->model('admin_model');
		$this->load->library('plivo');
    }
	// End of the function
	
	// Start of Generate Code script
	public function generate_code($length) {
		$CharSet1 	= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$CharSet2	= "0123456789";
        $String1 	= substr(str_shuffle($CharSet1), 0, (int)($length/2));
		$String2 	= substr(str_shuffle($CharSet2), 0, (int)($length/2));
		$code 		= $String1 . $String2;
		return $code;
	}
	// End of the function
	
	// Start of logout script
    public function logout() {
		$newdata = array(
			'UserID'  	=> '',
			'UserEmail'	=> '',
			'UserName'	=> '',
			'Loggedin' 	=> FALSE,
		);
        $this->session->unset_userdata($newdata);
        $this->session->sess_destroy();
        redirect(base_url());
        return FALSE;
    }
    // End of the function
	
	// Start of Send SMS Script
	public function send_sms($to, $message) {
		$this->not_logged_in();
		
		$sms_data = array(
			'src' => '+13303662824', 
			'dst' => $to,
			'text' => $message,
			'type' => 'sms',
		);
		$response_array = $this->plivo->send_sms($sms_data);
		if ($response_array[0] == '202') {
			$data["response"] = json_decode($response_array[1], TRUE);
			return $data["response"];
		} else {
			$this->api_error($response_array);
		}
	}
	// End of the function
	
	// Start of not logged in script
    public function not_logged_in() {
        if (($this->session->userdata('Loggedin') == FALSE)) {
            redirect(base_url());
            return FALSE;
        }
    }
    // End of the function
	
    // Start of logged in script
    public function logged_in() {
        if (($this->session->userdata('Loggedin') == TRUE)) {
            redirect(base_url('admin/members') . '/');
            return FALSE;
        }
    }
    // End of the function
}