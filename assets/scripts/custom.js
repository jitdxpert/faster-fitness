var base_url = $('#base_url').val();

$(function(){
	if ( $(".BootSwitch").length > 0 ) {
		$(".BootSwitch").bootstrapSwitch('state', false);
	}
});

$(document).ready(function() {
	"use strict";
	
	if ( $('.dataTable').length > 0 ) {
		$('.dataTable').DataTable({
			"oLanguage": {
				"oPaginate": {
					"sFirst": '<i class="fa fa-fast-backward"></i>',
					"sPrevious": '<i class="fa fa-backward"></i>',
					"sNext": '<i class="fa fa-forward"></i>',
					"sLast": '<i class="fa fa-fast-forward"></i>'
				}
			},
			aoColumnDefs: [{
				bSortable: false,
				aTargets: [ -1 ]
			}]
		});
	}
	
	if ( $(".BootSwitch").length > 0 ) {
		$('.BootSwitch').on('switchChange.bootstrapSwitch', function (e, data) {
			var parent = $(this).parents('.ExerciseRow');
			var td = $(this).parents('td');
			
			var EID = parent.find('input[name="EID"]').val();
			var GID = parent.find('input[name="GID"]').val();
			var state = $(this).bootstrapSwitch('state');
			var day = td.find('input[name="days"]').val();
			$.ajax({
				url 	: base_url + 'admin/save-targets/',
				type	: 'POST',
				data 	: 'EID=' + EID + '&GID=' + GID + '&state=' + state + '&day=' + day,
				success	: function(data) {
					// 
				}
			});
		});
		
		$('.BootSwitch').each(function() {
			var elem = $(this);
			var parent = $(this).parents('.ExerciseRow');
			var td = $(this).parents('td');
			
			var EID = parent.find('input[name="EID"]').val();
			var GID = parent.find('input[name="GID"]').val();
			var day = td.find('input[name="days"]').val();
			$.ajax({
				url 	: base_url + 'admin/get-targets-exercise/',
				type	: 'POST',
				data 	: 'EID=' + EID + '&GID=' + GID + '&day=' + day,
				success	: function(data) {
					if ( data == 1 ) {
						elem.bootstrapSwitch('state', true);
					} else {
						elem.bootstrapSwitch('state', false);
					}
				}
			});			
		});
	}
	
	$(".ex_model").click(function(){
		var exercise_id = $(this).attr('data-value');
		$.ajax({
			url 	: base_url + 'admin/get-single-exercise/',
			type	: 'POST',
			data 	: 'exercise_id=' + exercise_id,
			success	: function(data) {
				$('#ExerciseModal').html(data);
				$('#ExerciseModal').modal('show');
			}
		});
	});
	
	$(".point_model").click(function(){
		var earnpoint_id = $(this).attr('data-value');
		$.ajax({
			url 	: base_url + 'admin/get-single-point/',
			type	: 'POST',
			data 	: 'earnpoint_id=' + earnpoint_id,
			success	: function(data) {
				$('#pointModal').html(data);
				$('#pointModal').modal('show');
			}
		});
	});
	
	$(".gr_ex_model").click(function(){
	    var exercise_id = $(this).attr('data-exercise');
		var group_id = $(this).attr('data-group');
		$.ajax({
			url 	: base_url + 'admin/edit_group_exercise/',
			type	: 'POST',
			data 	: 'exercise_id=' + exercise_id+'&group_id='+group_id,
			success	: function(data) {
				$('#GroupExerciseModal').html(data);
				$('#GroupExerciseModal').modal('show');
			}
		});	
	});
	
	$(".member_model").click(function(){
        var emember_id = $(this).attr('data-value');
		$.ajax({
			url 	: base_url + 'admin/edit_single_member/',
			type	: 'POST',
			data 	: 'emember_id=' + emember_id,
			success	: function(data) {
				$('#memberModal').html(data);
				$('#memberModal').modal('show');
			}
		})
	});	
	
	$(".payment_model").click(function(){
        var rmember_id = $(this).attr('data-value');
		$.ajax({
			url 	: base_url + 'admin/edit_payment_status/',
			type	: 'POST',
			data 	: 'member_id=' + rmember_id,
			success	: function(data) {
				//alert(data);
				$('#paymentModal').html(data);
				$('#paymentModal').modal('show');
				$("#get_days").click(function(){
					var exdate = $('#nextexpirydays').val();
					var MID = $('#MID').val();
					$.ajax({
					url 	: base_url + 'admin/get_days_todate/',
					type	: 'POST',
					data 	: 'exdate=' + exdate+'&MID='+MID,
					success	: function(data) {
						if(data !="No"){
							if($('.expiry').hasClass('hide')){
								$('.expiry').removeClass('hide');
								$('#date_expire').val(data);
							}
						}
                        if($('#nextexpirydays').val()==""){
							if($('.expiry').hasClass('hide')){
							} else {
							$('.expiry').addClass('hide');	
							}
						} 						
						
					}
					})
				});
			}
		})
	});
	
	$(".weight_model").click(function(){
        var emember_id = $(this).attr('data-value');
		$.ajax({
			url 	: base_url + 'admin/edit_member_weight/',
			type	: 'POST',
			data 	: 'emember_id=' + emember_id,
			success	: function(data) {
				$('#weightModal').html(data);
				$('#weightModal').modal('show');
			}
		})		
	});	
	
	$(".performance_model").click(function(){
		var pmember_id = $(this).attr('data-value');
		//alert(pmember_id);
         $.ajax({
			url 	: base_url + 'admin/approve_performance/',
			type	: 'POST',
			data 	: 'member_id=' + pmember_id,
			success	: function(data) {
				//alert(data);
				$('#PerformanceModal').html(data);
				$('#PerformanceModal').modal('show');
			}
		})		
	});
	
	
	$(".performance_model").click(function(){
		var pmember_id = $(this).attr('data-value');
		$.ajax({
			url  : base_url + 'admin/approve_performance/',
			type : 'POST',
			data  : 'member_id=' + pmember_id,
			success : function(data) {
				$('#PerformanceModal').html(data);
				$('#PerformanceModal').modal('show');
			}
		});
	});
	
	
	$("#get_app_data").click(function(){
        var app_code = $(this).prev().val();
		$.ajax({
			url 	: base_url + 'admin/referral-code-details/',
			type	: 'POST',
			data 	: 'app_code=' + app_code,
			success	: function(data) {
				if(data == "No") {
					if($('.text-success').removeClass('hide')){
						$('.text-success').addClass('hide');
					}
					if($('.text-danger').hasClass('hide')){
						$('.text-danger').removeClass('hide');
					}
					$('#RefUserID').val('');
					$('#RefferUserID').val('');
					$('#RequestID').val('');
					$('#newAddID').val('');
					return false;
				} else {
					if($('.text-danger').removeClass('hide')){
						$('.text-danger').addClass('hide');
					}
					if($('.text-success').hasClass('hide')){
						$('.text-success').removeClass('hide');
					}
					var str = data.split("|");
					$('#RefUserID').val(str[0]);
					$('#RefferUserID').val(str[1]);
					$('#RequestID').val(str[2]);
					$('#RefStatus').val(str[3]);
					$('#newAddID').val(str[4]);
				}
			}
		});	
    });
	
	if ( $('#MName').length > 0 ) {
		$('#MName').autocomplete(base_url + 'admin/search-member-details/', {
			matchContains: true,
			mustMatch: false,
			minChars: 0,
			multiple: false,
			highlight: false,
			selectFirst: false
		});
	}	
	
});


$(function(){
	init_sidebar();
	init_login_center();
	init_toggles();
	
	$('[data-provide="datepicker"]').datepicker({
		//endDate: '+0d',
        autoclose : true,
    });
	
	$("#body-overlay").click(function () {
		$('#sidebar-exp').click();
	});
	
	
	$('.panel-default').each(function(){
		if ($('.panel-collapse:hidden', this).size()){
			$(this).closest('.panel-default').find('.panel-title a').addClass('collapsed');
		}
	});
	
	$(window).resize(function(){
		init_login_center();
	});
	
});

function init_login_center(){
	$('#login').css({
		position:'absolute',
		left: ($(window).width() - $('#login').outerWidth())/2,
		top: ($(window).height() - $('#login').outerHeight())/2
	});
}

function init_sidebar() {
	$('#sidebar-exp').click(function(){
		if ($('#wrapper').hasClass('sidebar')){
			$('#wrapper').removeClass('sidebar');
			$('#body-overlay').fadeOut('160');
		}else{
			$('#body-overlay').fadeIn('160');
			$('#wrapper').addClass('sidebar');
		}
		console.log('Expand sidebar');
		return false;    
	
	});
	
	$("#sidebar .drop-area>li").click(function(){
		$('#sidebar .selected').removeClass('selected');
		if ($('ul:visible', this).size()){
			$('ul:visible', this).slideUp('fast').removeClass('active');
		}else{
			$('#sidebar .drop-area>li>ul:visible').slideUp('fast').closest('li').removeClass('active');
			$(this).addClass('selected');
			$('ul:hidden', this).slideDown('fast').addClass('active');
		}
	});
	
}

function init_toggles(){
	if(typeof(window.Toggles) == "undefined"){
		console.log('Error - Toggles script is not included!');
		return false;
	}
	$('.toggle.on').toggles({on:true});
	$('.toggle.off').toggles({on:false});
}